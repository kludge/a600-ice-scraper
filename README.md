# A600 Ice Scraper

An DIP64 breakout board for the Amiga 600. It has no logic on board for disabling the Amiga 600 onboard CPU, so you sadly can't connect a 68000 or 68010 to it. You probably can't add an accelerator card either, unless it has functionality on it for disabling the Amiga 600 CPU.

The posted gerber files have not yet been tested. An earlier revision has however been produced without problems.

![A600 Ice Scraper](Images/A600_Ice_Scraper_Top_Large.png)