<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="yes" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="yes" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="yes" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="yes" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="yes" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="yes" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="yes" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="yes" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="yes" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="yes" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="yes" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="yes" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="yes" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="yes" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="yes" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="yes" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="yes" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="yes" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="yes" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="yes" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="yes" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="yes" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="yes" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="yes" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="yes" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="yes" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="yes" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="yes" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="yes" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="micro-mc68000">
<description>&lt;b&gt;Motorola MC68000 Processors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="PLCC68">
<description>&lt;b&gt;PLASTIC LEADED CHIP CARRIER&lt;/b&gt;&lt;p&gt;
square</description>
<wire x1="11.6" y1="12.1" x2="12.1" y2="11.6" width="0.1524" layer="51"/>
<wire x1="12.1" y1="11.6" x2="12.1" y2="-11.6" width="0.1524" layer="51"/>
<wire x1="12.1" y1="-11.6" x2="11.6" y2="-12.1" width="0.1524" layer="51"/>
<wire x1="11.6" y1="-12.1" x2="-11.6" y2="-12.1" width="0.1524" layer="51"/>
<wire x1="-11.6" y1="-12.1" x2="-12.1" y2="-11.6" width="0.1524" layer="51"/>
<wire x1="-12.1" y1="-11.6" x2="-12.1" y2="10.85" width="0.1524" layer="51"/>
<wire x1="-12.1" y1="10.85" x2="-10.54" y2="12.1" width="0.1524" layer="51"/>
<wire x1="-10.54" y1="12.1" x2="11.6" y2="12.1" width="0.1524" layer="51"/>
<wire x1="11.6" y1="-11.35" x2="11.35" y2="-11.6" width="0.0508" layer="51"/>
<wire x1="11.35" y1="-11.6" x2="-11.35" y2="-11.6" width="0.0508" layer="51"/>
<wire x1="-11.35" y1="-11.6" x2="-11.6" y2="-11.35" width="0.0508" layer="51"/>
<wire x1="-11.6" y1="-11.35" x2="-11.6" y2="10.85" width="0.0508" layer="51"/>
<wire x1="-11.6" y1="10.85" x2="-10.54" y2="12.1" width="0.0508" layer="51"/>
<wire x1="-12.1" y1="10.85" x2="-11.6" y2="10.85" width="0.0508" layer="51"/>
<wire x1="-11.6" y1="10.85" x2="11.6" y2="10.85" width="0.0508" layer="51"/>
<wire x1="11.6" y1="10.85" x2="11.6" y2="-11.35" width="0.0508" layer="51"/>
<wire x1="12.1" y1="11.6" x2="11.6" y2="10.85" width="0.0508" layer="51"/>
<wire x1="11.6" y1="-11.35" x2="12.1" y2="-11.6" width="0.0508" layer="51"/>
<wire x1="11.35" y1="-11.6" x2="11.6" y2="-12.1" width="0.0508" layer="51"/>
<wire x1="-12.1" y1="-11.6" x2="-11.6" y2="-11.35" width="0.0508" layer="51"/>
<wire x1="-11.6" y1="-12.1" x2="-11.35" y2="-11.6" width="0.0508" layer="51"/>
<wire x1="12.1" y1="9.669" x2="12.1" y2="9.381" width="0.1524" layer="21"/>
<wire x1="12.1" y1="8.399" x2="12.1" y2="8.111" width="0.1524" layer="21"/>
<wire x1="12.1" y1="7.129" x2="12.1" y2="6.841" width="0.1524" layer="21"/>
<wire x1="12.1" y1="5.859" x2="12.1" y2="5.571" width="0.1524" layer="21"/>
<wire x1="12.1" y1="4.589" x2="12.1" y2="4.301" width="0.1524" layer="21"/>
<wire x1="12.1" y1="3.319" x2="12.1" y2="3.031" width="0.1524" layer="21"/>
<wire x1="12.1" y1="-10.651" x2="12.1" y2="-11.6" width="0.1524" layer="21"/>
<wire x1="12.1" y1="-11.6" x2="11.6" y2="-12.1" width="0.1524" layer="21"/>
<wire x1="11.6" y1="-12.1" x2="10.651" y2="-12.1" width="0.1524" layer="21"/>
<wire x1="7.129" y1="-12.1" x2="6.841" y2="-12.1" width="0.1524" layer="21"/>
<wire x1="5.859" y1="-12.1" x2="5.571" y2="-12.1" width="0.1524" layer="21"/>
<wire x1="4.589" y1="-12.1" x2="4.301" y2="-12.1" width="0.1524" layer="21"/>
<wire x1="3.319" y1="-12.1" x2="3.031" y2="-12.1" width="0.1524" layer="21"/>
<wire x1="2.049" y1="-12.1" x2="1.761" y2="-12.1" width="0.1524" layer="21"/>
<wire x1="0.779" y1="-12.1" x2="0.491" y2="-12.1" width="0.1524" layer="21"/>
<wire x1="-10.651" y1="-12.1" x2="-11.6" y2="-12.1" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="-12.1" x2="-12.1" y2="-11.6" width="0.1524" layer="21"/>
<wire x1="-12.1" y1="-11.6" x2="-12.1" y2="-10.651" width="0.1524" layer="21"/>
<wire x1="-12.1" y1="3.031" x2="-12.1" y2="3.319" width="0.1524" layer="21"/>
<wire x1="-12.1" y1="4.301" x2="-12.1" y2="4.589" width="0.1524" layer="21"/>
<wire x1="-12.1" y1="5.571" x2="-12.1" y2="5.859" width="0.1524" layer="21"/>
<wire x1="-12.1" y1="6.841" x2="-12.1" y2="7.129" width="0.1524" layer="21"/>
<wire x1="-12.1" y1="8.111" x2="-12.1" y2="8.399" width="0.1524" layer="21"/>
<wire x1="-12.1" y1="9.381" x2="-12.1" y2="9.669" width="0.1524" layer="21"/>
<wire x1="-12.1" y1="10.651" x2="-12.1" y2="10.85" width="0.1524" layer="21"/>
<wire x1="-12.1" y1="10.85" x2="-10.651" y2="11.999" width="0.1524" layer="21"/>
<wire x1="0.491" y1="12.1" x2="0.779" y2="12.1" width="0.1524" layer="21"/>
<wire x1="1.761" y1="12.1" x2="2.049" y2="12.1" width="0.1524" layer="21"/>
<wire x1="2.98" y1="12.1" x2="3.37" y2="12.1" width="0.1524" layer="21"/>
<wire x1="4.301" y1="12.1" x2="4.589" y2="12.1" width="0.1524" layer="21"/>
<wire x1="5.571" y1="12.1" x2="5.859" y2="12.1" width="0.1524" layer="21"/>
<wire x1="6.841" y1="12.1" x2="7.129" y2="12.1" width="0.1524" layer="21"/>
<wire x1="10.651" y1="12.1" x2="11.6" y2="12.1" width="0.1524" layer="21"/>
<wire x1="11.6" y1="12.1" x2="12.1" y2="11.6" width="0.1524" layer="21"/>
<wire x1="12.1" y1="11.6" x2="12.1" y2="10.651" width="0.1524" layer="21"/>
<wire x1="11.6" y1="10.85" x2="11.6" y2="10.611" width="0.0508" layer="21"/>
<wire x1="11.6" y1="9.709" x2="11.6" y2="9.341" width="0.0508" layer="21"/>
<wire x1="11.6" y1="8.439" x2="11.6" y2="8.071" width="0.0508" layer="21"/>
<wire x1="11.6" y1="7.169" x2="11.6" y2="6.801" width="0.0508" layer="21"/>
<wire x1="11.6" y1="5.899" x2="11.6" y2="5.531" width="0.0508" layer="21"/>
<wire x1="11.6" y1="4.629" x2="11.6" y2="4.261" width="0.0508" layer="21"/>
<wire x1="11.6" y1="3.359" x2="11.6" y2="2.991" width="0.0508" layer="21"/>
<wire x1="11.6" y1="-10.611" x2="11.6" y2="-11.35" width="0.0508" layer="21"/>
<wire x1="11.6" y1="-11.35" x2="11.35" y2="-11.6" width="0.0508" layer="21"/>
<wire x1="11.35" y1="-11.6" x2="10.611" y2="-11.6" width="0.0508" layer="21"/>
<wire x1="7.169" y1="-11.6" x2="6.801" y2="-11.6" width="0.0508" layer="21"/>
<wire x1="5.899" y1="-11.6" x2="5.531" y2="-11.6" width="0.0508" layer="21"/>
<wire x1="4.629" y1="-11.6" x2="4.261" y2="-11.6" width="0.0508" layer="21"/>
<wire x1="3.359" y1="-11.6" x2="2.991" y2="-11.6" width="0.0508" layer="21"/>
<wire x1="2.089" y1="-11.6" x2="1.721" y2="-11.6" width="0.0508" layer="21"/>
<wire x1="0.819" y1="-11.6" x2="0.451" y2="-11.6" width="0.0508" layer="21"/>
<wire x1="-10.611" y1="-11.6" x2="-11.35" y2="-11.6" width="0.0508" layer="21"/>
<wire x1="-11.35" y1="-11.6" x2="-11.6" y2="-11.35" width="0.0508" layer="21"/>
<wire x1="-11.6" y1="-11.35" x2="-11.6" y2="-10.611" width="0.0508" layer="21"/>
<wire x1="-11.6" y1="2.991" x2="-11.6" y2="3.359" width="0.0508" layer="21"/>
<wire x1="-11.6" y1="4.261" x2="-11.6" y2="4.629" width="0.0508" layer="21"/>
<wire x1="-11.6" y1="5.531" x2="-11.6" y2="5.899" width="0.0508" layer="21"/>
<wire x1="-11.6" y1="6.801" x2="-11.6" y2="7.169" width="0.0508" layer="21"/>
<wire x1="-11.6" y1="8.071" x2="-11.6" y2="8.439" width="0.0508" layer="21"/>
<wire x1="-11.6" y1="9.341" x2="-11.6" y2="9.709" width="0.0508" layer="21"/>
<wire x1="-11.6" y1="10.611" x2="-11.6" y2="10.85" width="0.0508" layer="21"/>
<wire x1="-11.6" y1="10.85" x2="-10.611" y2="12.029" width="0.0508" layer="21"/>
<wire x1="-11.6" y1="10.85" x2="-10.611" y2="10.85" width="0.0508" layer="21"/>
<wire x1="0.451" y1="10.85" x2="0.819" y2="10.85" width="0.0508" layer="21"/>
<wire x1="1.721" y1="10.85" x2="2.089" y2="10.85" width="0.0508" layer="21"/>
<wire x1="2.94" y1="10.85" x2="3.41" y2="10.85" width="0.0508" layer="21"/>
<wire x1="4.261" y1="10.85" x2="4.629" y2="10.85" width="0.0508" layer="21"/>
<wire x1="5.531" y1="10.85" x2="5.899" y2="10.85" width="0.0508" layer="21"/>
<wire x1="6.801" y1="10.85" x2="7.169" y2="10.85" width="0.0508" layer="21"/>
<wire x1="10.611" y1="10.85" x2="11.6" y2="10.85" width="0.0508" layer="21"/>
<wire x1="12.1" y1="11.6" x2="11.6" y2="10.85" width="0.1524" layer="21"/>
<wire x1="11.35" y1="-11.6" x2="11.6" y2="-12.1" width="0.0508" layer="21"/>
<wire x1="11.6" y1="-11.35" x2="12.1" y2="-11.6" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="-12.1" x2="-11.35" y2="-11.6" width="0.0508" layer="21"/>
<wire x1="-12.1" y1="-11.6" x2="-11.6" y2="-11.35" width="0.1524" layer="21"/>
<wire x1="-12.1" y1="10.85" x2="-11.6" y2="10.85" width="0.0508" layer="21"/>
<wire x1="-11.6" y1="1.721" x2="-11.6" y2="2.089" width="0.0508" layer="21"/>
<wire x1="-11.6" y1="0.451" x2="-11.6" y2="0.819" width="0.0508" layer="21"/>
<wire x1="-12.1" y1="1.761" x2="-12.1" y2="2.049" width="0.1524" layer="21"/>
<wire x1="-12.1" y1="0.491" x2="-12.1" y2="0.779" width="0.1524" layer="21"/>
<wire x1="12.1" y1="2.049" x2="12.1" y2="1.761" width="0.1524" layer="21"/>
<wire x1="12.1" y1="0.779" x2="12.1" y2="0.491" width="0.1524" layer="21"/>
<wire x1="11.6" y1="2.089" x2="11.6" y2="1.721" width="0.0508" layer="21"/>
<wire x1="11.6" y1="0.819" x2="11.6" y2="0.451" width="0.0508" layer="21"/>
<wire x1="12.1" y1="-0.491" x2="12.1" y2="-0.779" width="0.1524" layer="21"/>
<wire x1="12.1" y1="-1.761" x2="12.1" y2="-2.049" width="0.1524" layer="21"/>
<wire x1="11.6" y1="-0.451" x2="11.6" y2="-0.819" width="0.0508" layer="21"/>
<wire x1="11.6" y1="-1.721" x2="11.6" y2="-2.089" width="0.0508" layer="21"/>
<wire x1="8.399" y1="-12.1" x2="8.111" y2="-12.1" width="0.1524" layer="21"/>
<wire x1="9.669" y1="-12.1" x2="9.381" y2="-12.1" width="0.1524" layer="21"/>
<wire x1="8.439" y1="-11.6" x2="8.071" y2="-11.6" width="0.0508" layer="21"/>
<wire x1="9.709" y1="-11.6" x2="9.341" y2="-11.6" width="0.0508" layer="21"/>
<wire x1="8.111" y1="12.1" x2="8.399" y2="12.1" width="0.1524" layer="21"/>
<wire x1="9.381" y1="12.1" x2="9.669" y2="12.1" width="0.1524" layer="21"/>
<wire x1="8.071" y1="10.85" x2="8.439" y2="10.85" width="0.0508" layer="21"/>
<wire x1="9.341" y1="10.85" x2="9.709" y2="10.85" width="0.0508" layer="21"/>
<wire x1="-0.779" y1="12.1" x2="-0.491" y2="12.1" width="0.1524" layer="21"/>
<wire x1="-2.049" y1="12.1" x2="-1.761" y2="12.1" width="0.1524" layer="21"/>
<wire x1="-0.819" y1="10.85" x2="-0.451" y2="10.85" width="0.0508" layer="21"/>
<wire x1="-2.089" y1="10.85" x2="-1.721" y2="10.85" width="0.0508" layer="21"/>
<wire x1="-0.491" y1="-12.1" x2="-0.779" y2="-12.1" width="0.1524" layer="21"/>
<wire x1="-1.761" y1="-12.1" x2="-2.049" y2="-12.1" width="0.1524" layer="21"/>
<wire x1="-0.451" y1="-11.6" x2="-0.819" y2="-11.6" width="0.0508" layer="21"/>
<wire x1="-1.721" y1="-11.6" x2="-2.089" y2="-11.6" width="0.0508" layer="21"/>
<wire x1="-11.6" y1="-0.819" x2="-11.6" y2="-0.451" width="0.0508" layer="21"/>
<wire x1="-11.6" y1="-2.089" x2="-11.6" y2="-1.721" width="0.0508" layer="21"/>
<wire x1="-12.1" y1="-0.779" x2="-12.1" y2="-0.491" width="0.1524" layer="21"/>
<wire x1="-12.1" y1="-2.049" x2="-12.1" y2="-1.761" width="0.1524" layer="21"/>
<wire x1="-3.031" y1="-12.1" x2="-3.319" y2="-12.1" width="0.1524" layer="21"/>
<wire x1="-4.301" y1="-12.1" x2="-4.589" y2="-12.1" width="0.1524" layer="21"/>
<wire x1="-5.571" y1="-12.1" x2="-5.859" y2="-12.1" width="0.1524" layer="21"/>
<wire x1="-6.841" y1="-12.1" x2="-7.129" y2="-12.1" width="0.1524" layer="21"/>
<wire x1="-8.111" y1="-12.1" x2="-8.399" y2="-12.1" width="0.1524" layer="21"/>
<wire x1="-9.381" y1="-12.1" x2="-9.669" y2="-12.1" width="0.1524" layer="21"/>
<wire x1="-2.991" y1="-11.6" x2="-3.359" y2="-11.6" width="0.0508" layer="21"/>
<wire x1="-4.261" y1="-11.6" x2="-4.629" y2="-11.6" width="0.0508" layer="21"/>
<wire x1="-5.531" y1="-11.6" x2="-5.899" y2="-11.6" width="0.0508" layer="21"/>
<wire x1="-6.801" y1="-11.6" x2="-7.169" y2="-11.6" width="0.0508" layer="21"/>
<wire x1="-8.071" y1="-11.6" x2="-8.439" y2="-11.6" width="0.0508" layer="21"/>
<wire x1="-9.341" y1="-11.6" x2="-9.709" y2="-11.6" width="0.0508" layer="21"/>
<wire x1="-3.319" y1="12.1" x2="-3.031" y2="12.1" width="0.1524" layer="21"/>
<wire x1="-4.589" y1="12.1" x2="-4.301" y2="12.1" width="0.1524" layer="21"/>
<wire x1="-5.859" y1="12.1" x2="-5.571" y2="12.1" width="0.1524" layer="21"/>
<wire x1="-7.129" y1="12.1" x2="-6.841" y2="12.1" width="0.1524" layer="21"/>
<wire x1="-8.399" y1="12.1" x2="-8.111" y2="12.1" width="0.1524" layer="21"/>
<wire x1="-9.669" y1="12.1" x2="-9.381" y2="12.1" width="0.1524" layer="21"/>
<wire x1="-3.359" y1="10.85" x2="-2.991" y2="10.85" width="0.0508" layer="21"/>
<wire x1="-4.629" y1="10.85" x2="-4.261" y2="10.85" width="0.0508" layer="21"/>
<wire x1="-5.899" y1="10.85" x2="-5.531" y2="10.85" width="0.0508" layer="21"/>
<wire x1="-7.169" y1="10.85" x2="-6.801" y2="10.85" width="0.0508" layer="21"/>
<wire x1="-8.439" y1="10.85" x2="-8.071" y2="10.85" width="0.0508" layer="21"/>
<wire x1="-9.709" y1="10.85" x2="-9.341" y2="10.85" width="0.0508" layer="21"/>
<wire x1="12.1" y1="-3.031" x2="12.1" y2="-3.319" width="0.1524" layer="21"/>
<wire x1="12.1" y1="-4.301" x2="12.1" y2="-4.589" width="0.1524" layer="21"/>
<wire x1="12.1" y1="-5.571" x2="12.1" y2="-5.859" width="0.1524" layer="21"/>
<wire x1="12.1" y1="-6.841" x2="12.1" y2="-7.129" width="0.1524" layer="21"/>
<wire x1="12.1" y1="-8.111" x2="12.1" y2="-8.399" width="0.1524" layer="21"/>
<wire x1="11.6" y1="-2.991" x2="11.6" y2="-3.359" width="0.0508" layer="21"/>
<wire x1="11.6" y1="-4.261" x2="11.6" y2="-4.629" width="0.0508" layer="21"/>
<wire x1="11.6" y1="-5.531" x2="11.6" y2="-5.899" width="0.0508" layer="21"/>
<wire x1="11.6" y1="-6.801" x2="11.6" y2="-7.169" width="0.0508" layer="21"/>
<wire x1="11.6" y1="-8.071" x2="11.6" y2="-8.439" width="0.0508" layer="21"/>
<wire x1="-12.1" y1="-3.319" x2="-12.1" y2="-3.031" width="0.1524" layer="21"/>
<wire x1="-12.1" y1="-4.589" x2="-12.1" y2="-4.301" width="0.1524" layer="21"/>
<wire x1="-12.1" y1="-5.859" x2="-12.1" y2="-5.571" width="0.1524" layer="21"/>
<wire x1="-12.1" y1="-7.129" x2="-12.1" y2="-6.841" width="0.1524" layer="21"/>
<wire x1="-12.1" y1="-8.399" x2="-12.1" y2="-8.111" width="0.1524" layer="21"/>
<wire x1="-11.6" y1="-3.359" x2="-11.6" y2="-2.991" width="0.0508" layer="21"/>
<wire x1="-11.6" y1="-4.629" x2="-11.6" y2="-4.261" width="0.0508" layer="21"/>
<wire x1="-11.6" y1="-5.899" x2="-11.6" y2="-5.531" width="0.0508" layer="21"/>
<wire x1="-11.6" y1="-7.169" x2="-11.6" y2="-6.801" width="0.0508" layer="21"/>
<wire x1="-11.6" y1="-8.439" x2="-11.6" y2="-8.071" width="0.0508" layer="21"/>
<wire x1="12.1" y1="-9.381" x2="12.1" y2="-9.669" width="0.1524" layer="21"/>
<wire x1="11.6" y1="-9.341" x2="11.6" y2="-9.709" width="0.0508" layer="21"/>
<wire x1="-11.6" y1="-9.709" x2="-11.6" y2="-9.341" width="0.0508" layer="21"/>
<wire x1="-12.1" y1="-9.669" x2="-12.1" y2="-9.381" width="0.1524" layer="21"/>
<circle x="0" y="9.77" radius="0.5" width="0.0508" layer="51"/>
<circle x="0" y="9.77" radius="0.5" width="0.0508" layer="21"/>
<smd name="14" x="-11.67" y="5.08" dx="1.8" dy="0.76" layer="1"/>
<smd name="15" x="-11.67" y="3.81" dx="1.8" dy="0.76" layer="1"/>
<smd name="16" x="-11.67" y="2.54" dx="1.8" dy="0.76" layer="1"/>
<smd name="17" x="-11.67" y="1.27" dx="1.8" dy="0.76" layer="1"/>
<smd name="18" x="-11.67" y="0" dx="1.8" dy="0.76" layer="1"/>
<smd name="19" x="-11.67" y="-1.27" dx="1.8" dy="0.76" layer="1"/>
<smd name="20" x="-11.67" y="-2.54" dx="1.8" dy="0.76" layer="1"/>
<smd name="54" x="11.67" y="2.54" dx="1.8" dy="0.76" layer="1"/>
<smd name="53" x="11.67" y="1.27" dx="1.8" dy="0.76" layer="1"/>
<smd name="52" x="11.67" y="0" dx="1.8" dy="0.76" layer="1"/>
<smd name="51" x="11.67" y="-1.27" dx="1.8" dy="0.76" layer="1"/>
<smd name="50" x="11.67" y="-2.54" dx="1.8" dy="0.76" layer="1"/>
<smd name="49" x="11.67" y="-3.81" dx="1.8" dy="0.76" layer="1"/>
<smd name="48" x="11.67" y="-5.08" dx="1.8" dy="0.76" layer="1"/>
<smd name="9" x="-10.16" y="11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="3" x="-2.54" y="11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="2" x="-1.27" y="11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="1" x="0" y="11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="68" x="1.27" y="11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="67" x="2.54" y="11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="66" x="3.81" y="11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="27" x="-10.16" y="-11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="33" x="-2.54" y="-11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="34" x="-1.27" y="-11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="35" x="0" y="-11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="36" x="1.27" y="-11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="37" x="2.54" y="-11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="38" x="3.81" y="-11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="21" x="-11.67" y="-3.81" dx="1.8" dy="0.76" layer="1"/>
<smd name="22" x="-11.67" y="-5.08" dx="1.8" dy="0.76" layer="1"/>
<smd name="55" x="11.67" y="3.81" dx="1.8" dy="0.76" layer="1"/>
<smd name="56" x="11.67" y="5.08" dx="1.8" dy="0.76" layer="1"/>
<smd name="47" x="11.67" y="-6.35" dx="1.8" dy="0.76" layer="1"/>
<smd name="39" x="5.08" y="-11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="40" x="6.35" y="-11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="32" x="-3.81" y="-11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="31" x="-5.08" y="-11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="23" x="-11.67" y="-6.35" dx="1.8" dy="0.76" layer="1"/>
<smd name="4" x="-3.81" y="11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="5" x="-5.08" y="11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="65" x="5.08" y="11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="64" x="6.35" y="11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="57" x="11.67" y="6.35" dx="1.8" dy="0.76" layer="1"/>
<smd name="13" x="-11.67" y="6.35" dx="1.8" dy="0.76" layer="1"/>
<smd name="30" x="-6.35" y="-11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="29" x="-7.62" y="-11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="28" x="-8.89" y="-11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="24" x="-11.67" y="-7.62" dx="1.8" dy="0.76" layer="1"/>
<smd name="25" x="-11.67" y="-8.89" dx="1.8" dy="0.76" layer="1"/>
<smd name="26" x="-11.67" y="-10.16" dx="1.8" dy="0.76" layer="1"/>
<smd name="12" x="-11.67" y="7.62" dx="1.8" dy="0.76" layer="1"/>
<smd name="11" x="-11.67" y="8.89" dx="1.8" dy="0.76" layer="1"/>
<smd name="10" x="-11.67" y="10.16" dx="1.8" dy="0.76" layer="1"/>
<smd name="6" x="-6.35" y="11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="7" x="-7.62" y="11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="8" x="-8.89" y="11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="58" x="11.67" y="7.62" dx="1.8" dy="0.76" layer="1"/>
<smd name="59" x="11.67" y="8.89" dx="1.8" dy="0.76" layer="1"/>
<smd name="60" x="11.67" y="10.16" dx="1.8" dy="0.76" layer="1"/>
<smd name="63" x="7.62" y="11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="62" x="8.89" y="11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="61" x="10.16" y="11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="46" x="11.67" y="-7.62" dx="1.8" dy="0.76" layer="1"/>
<smd name="45" x="11.67" y="-8.89" dx="1.8" dy="0.76" layer="1"/>
<smd name="44" x="11.67" y="-10.16" dx="1.8" dy="0.76" layer="1"/>
<smd name="41" x="7.62" y="-11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="42" x="8.89" y="-11.67" dx="0.76" dy="1.8" layer="1"/>
<smd name="43" x="10.16" y="-11.67" dx="0.76" dy="1.8" layer="1"/>
<text x="-10.414" y="12.954" size="1.778" layer="25">&gt;NAME</text>
<text x="-8.255" y="-1.905" size="1.778" layer="27">&gt;VALUE</text>
<rectangle x1="12.1" y1="2.16" x2="12.57" y2="2.92" layer="51"/>
<rectangle x1="12.1" y1="0.89" x2="12.57" y2="1.65" layer="51"/>
<rectangle x1="12.1" y1="-0.38" x2="12.57" y2="0.38" layer="51"/>
<rectangle x1="12.1" y1="-1.65" x2="12.57" y2="-0.89" layer="51"/>
<rectangle x1="12.1" y1="-2.92" x2="12.57" y2="-2.16" layer="51"/>
<rectangle x1="12.1" y1="-4.19" x2="12.57" y2="-3.43" layer="51"/>
<rectangle x1="12.1" y1="-5.46" x2="12.57" y2="-4.7" layer="51"/>
<rectangle x1="3.43" y1="-12.57" x2="4.19" y2="-12.1" layer="51"/>
<rectangle x1="2.16" y1="-12.57" x2="2.92" y2="-12.1" layer="51"/>
<rectangle x1="0.89" y1="-12.57" x2="1.65" y2="-12.1" layer="51"/>
<rectangle x1="-0.38" y1="-12.57" x2="0.38" y2="-12.1" layer="51"/>
<rectangle x1="-1.65" y1="-12.57" x2="-0.89" y2="-12.1" layer="51"/>
<rectangle x1="-2.92" y1="-12.57" x2="-2.16" y2="-12.1" layer="51"/>
<rectangle x1="-10.54" y1="-12.57" x2="-9.78" y2="-12.1" layer="51"/>
<rectangle x1="-12.57" y1="4.7" x2="-12.1" y2="5.46" layer="51"/>
<rectangle x1="-12.57" y1="3.43" x2="-12.1" y2="4.19" layer="51"/>
<rectangle x1="-12.57" y1="2.16" x2="-12.1" y2="2.92" layer="51"/>
<rectangle x1="-12.57" y1="0.89" x2="-12.1" y2="1.65" layer="51"/>
<rectangle x1="-12.57" y1="-0.38" x2="-12.1" y2="0.38" layer="51"/>
<rectangle x1="-12.57" y1="-1.65" x2="-12.1" y2="-0.89" layer="51"/>
<rectangle x1="-12.57" y1="-2.92" x2="-12.1" y2="-2.16" layer="51"/>
<rectangle x1="3.43" y1="12.1" x2="4.19" y2="12.57" layer="51"/>
<rectangle x1="2.16" y1="12.1" x2="2.92" y2="12.57" layer="51"/>
<rectangle x1="0.89" y1="12.1" x2="1.65" y2="12.57" layer="51"/>
<rectangle x1="-0.38" y1="12.1" x2="0.38" y2="12.57" layer="51"/>
<rectangle x1="-1.65" y1="12.1" x2="-0.89" y2="12.57" layer="51"/>
<rectangle x1="-2.92" y1="12.1" x2="-2.16" y2="12.57" layer="51"/>
<rectangle x1="-10.54" y1="12.1" x2="-9.78" y2="12.57" layer="51"/>
<rectangle x1="12.1" y1="3.43" x2="12.57" y2="4.19" layer="51"/>
<rectangle x1="12.1" y1="4.7" x2="12.57" y2="5.46" layer="51"/>
<rectangle x1="-12.57" y1="-4.19" x2="-12.1" y2="-3.43" layer="51"/>
<rectangle x1="-12.57" y1="-5.46" x2="-12.1" y2="-4.7" layer="51"/>
<rectangle x1="4.7" y1="12.1" x2="5.46" y2="12.57" layer="51"/>
<rectangle x1="5.97" y1="12.1" x2="6.73" y2="12.57" layer="51"/>
<rectangle x1="-4.19" y1="12.1" x2="-3.43" y2="12.57" layer="51"/>
<rectangle x1="-5.46" y1="12.1" x2="-4.7" y2="12.57" layer="51"/>
<rectangle x1="-12.57" y1="-6.73" x2="-12.1" y2="-5.97" layer="51"/>
<rectangle x1="-5.46" y1="-12.57" x2="-4.7" y2="-12.1" layer="51"/>
<rectangle x1="-4.19" y1="-12.57" x2="-3.43" y2="-12.1" layer="51"/>
<rectangle x1="4.7" y1="-12.57" x2="5.46" y2="-12.1" layer="51"/>
<rectangle x1="5.97" y1="-12.57" x2="6.73" y2="-12.1" layer="51"/>
<rectangle x1="12.1" y1="-6.73" x2="12.57" y2="-5.97" layer="51"/>
<rectangle x1="12.1" y1="5.97" x2="12.57" y2="6.73" layer="51"/>
<rectangle x1="-12.57" y1="5.97" x2="-12.1" y2="6.73" layer="51"/>
<rectangle x1="-6.73" y1="-12.57" x2="-5.97" y2="-12.1" layer="51"/>
<rectangle x1="-8" y1="-12.57" x2="-7.24" y2="-12.1" layer="51"/>
<rectangle x1="-9.27" y1="-12.57" x2="-8.51" y2="-12.1" layer="51"/>
<rectangle x1="-12.57" y1="-8" x2="-12.1" y2="-7.24" layer="51"/>
<rectangle x1="-12.57" y1="-9.27" x2="-12.1" y2="-8.51" layer="51"/>
<rectangle x1="-12.57" y1="-10.54" x2="-12.1" y2="-9.78" layer="51"/>
<rectangle x1="-12.57" y1="7.24" x2="-12.1" y2="8" layer="51"/>
<rectangle x1="-12.57" y1="8.51" x2="-12.1" y2="9.27" layer="51"/>
<rectangle x1="-12.57" y1="9.78" x2="-12.1" y2="10.54" layer="51"/>
<rectangle x1="-6.73" y1="12.1" x2="-5.97" y2="12.57" layer="51"/>
<rectangle x1="-8" y1="12.1" x2="-7.24" y2="12.57" layer="51"/>
<rectangle x1="-9.27" y1="12.1" x2="-8.51" y2="12.57" layer="51"/>
<rectangle x1="12.1" y1="7.24" x2="12.57" y2="8" layer="51"/>
<rectangle x1="12.1" y1="8.51" x2="12.57" y2="9.27" layer="51"/>
<rectangle x1="12.1" y1="9.78" x2="12.57" y2="10.54" layer="51"/>
<rectangle x1="7.24" y1="12.1" x2="8" y2="12.57" layer="51"/>
<rectangle x1="8.51" y1="12.1" x2="9.27" y2="12.57" layer="51"/>
<rectangle x1="9.78" y1="12.1" x2="10.54" y2="12.57" layer="51"/>
<rectangle x1="12.1" y1="-8" x2="12.57" y2="-7.24" layer="51"/>
<rectangle x1="12.1" y1="-9.27" x2="12.57" y2="-8.51" layer="51"/>
<rectangle x1="12.1" y1="-10.54" x2="12.57" y2="-9.78" layer="51"/>
<rectangle x1="7.24" y1="-12.57" x2="8" y2="-12.1" layer="51"/>
<rectangle x1="8.51" y1="-12.57" x2="9.27" y2="-12.1" layer="51"/>
<rectangle x1="9.78" y1="-12.57" x2="10.54" y2="-12.1" layer="51"/>
</package>
<package name="PLCC68-S">
<description>&lt;b&gt;PLCC Socked&lt;/b&gt;</description>
<wire x1="-13.97" y1="15.494" x2="-15.494" y2="13.97" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="13.97" x2="-15.494" y2="-14.859" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="10.795" x2="-10.795" y2="12.065" width="0.1524" layer="21"/>
<wire x1="-13.208" y1="-14.224" x2="-11.049" y2="-12.065" width="0.1524" layer="21"/>
<wire x1="-13.208" y1="-14.224" x2="-14.224" y2="-13.208" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="-13.208" x2="-12.065" y2="-11.049" width="0.1524" layer="21"/>
<wire x1="-11.049" y1="-12.065" x2="-10.795" y2="-12.065" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-12.065" x2="-4.191" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="-13.208" x2="-4.191" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="-13.208" x2="-4.191" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="-14.224" x2="-3.429" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-14.224" x2="-3.429" y2="-13.208" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-12.065" x2="-3.429" y2="-13.208" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="-12.065" x2="-2.921" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="-12.065" x2="-1.651" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="-13.208" x2="-1.651" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="-13.208" x2="-1.651" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="-14.224" x2="-0.889" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-14.224" x2="-0.889" y2="-13.208" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-12.065" x2="-0.889" y2="-13.208" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-12.065" x2="-0.381" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-12.065" x2="0.889" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-13.208" x2="0.889" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-13.208" x2="0.889" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-14.224" x2="1.651" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-14.224" x2="1.651" y2="-13.208" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-12.065" x2="1.651" y2="-13.208" width="0.1524" layer="51"/>
<wire x1="14.224" y1="13.208" x2="12.065" y2="11.049" width="0.1524" layer="21"/>
<wire x1="14.224" y1="13.208" x2="13.208" y2="14.224" width="0.1524" layer="21"/>
<wire x1="13.208" y1="14.224" x2="11.049" y2="12.065" width="0.1524" layer="21"/>
<wire x1="12.065" y1="11.049" x2="12.065" y2="10.795" width="0.1524" layer="21"/>
<wire x1="12.065" y1="4.699" x2="12.065" y2="4.191" width="0.1524" layer="51"/>
<wire x1="13.208" y1="4.191" x2="12.065" y2="4.191" width="0.1524" layer="51"/>
<wire x1="13.208" y1="4.191" x2="14.224" y2="4.191" width="0.1524" layer="21"/>
<wire x1="14.224" y1="4.191" x2="14.224" y2="3.429" width="0.1524" layer="21"/>
<wire x1="14.224" y1="3.429" x2="13.208" y2="3.429" width="0.1524" layer="21"/>
<wire x1="12.065" y1="3.429" x2="13.208" y2="3.429" width="0.1524" layer="51"/>
<wire x1="12.065" y1="3.429" x2="12.065" y2="2.921" width="0.1524" layer="51"/>
<wire x1="12.065" y1="-0.381" x2="12.065" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="13.208" y1="-0.889" x2="12.065" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="13.208" y1="-0.889" x2="14.224" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="14.224" y1="-0.889" x2="14.224" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="14.224" y1="-1.651" x2="13.208" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-1.651" x2="13.208" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="-12.065" y1="-11.049" x2="-12.065" y2="-10.795" width="0.1524" layer="21"/>
<wire x1="4.699" y1="12.065" x2="4.191" y2="12.065" width="0.1524" layer="51"/>
<wire x1="4.191" y1="13.208" x2="4.191" y2="12.065" width="0.1524" layer="51"/>
<wire x1="4.191" y1="13.208" x2="4.191" y2="14.224" width="0.1524" layer="21"/>
<wire x1="4.191" y1="14.224" x2="3.429" y2="14.224" width="0.1524" layer="21"/>
<wire x1="3.429" y1="14.224" x2="3.429" y2="13.208" width="0.1524" layer="21"/>
<wire x1="3.429" y1="12.065" x2="3.429" y2="13.208" width="0.1524" layer="51"/>
<wire x1="3.429" y1="12.065" x2="2.921" y2="12.065" width="0.1524" layer="51"/>
<wire x1="12.065" y1="2.159" x2="12.065" y2="1.651" width="0.1524" layer="51"/>
<wire x1="13.208" y1="1.651" x2="12.065" y2="1.651" width="0.1524" layer="51"/>
<wire x1="13.208" y1="1.651" x2="14.224" y2="1.651" width="0.1524" layer="21"/>
<wire x1="14.224" y1="1.651" x2="14.224" y2="0.889" width="0.1524" layer="21"/>
<wire x1="14.224" y1="0.889" x2="13.208" y2="0.889" width="0.1524" layer="21"/>
<wire x1="12.065" y1="0.889" x2="13.208" y2="0.889" width="0.1524" layer="51"/>
<wire x1="12.065" y1="0.889" x2="12.065" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-12.065" y1="-4.699" x2="-12.065" y2="-4.191" width="0.1524" layer="51"/>
<wire x1="-13.208" y1="-4.191" x2="-12.065" y2="-4.191" width="0.1524" layer="51"/>
<wire x1="-13.208" y1="-4.191" x2="-14.224" y2="-4.191" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="-4.191" x2="-14.224" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="-3.429" x2="-13.208" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-3.429" x2="-13.208" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="-12.065" y1="-3.429" x2="-12.065" y2="-2.921" width="0.1524" layer="51"/>
<wire x1="-12.065" y1="-2.159" x2="-12.065" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="-13.208" y1="-1.651" x2="-12.065" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="-13.208" y1="-1.651" x2="-14.224" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="-1.651" x2="-14.224" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="-0.889" x2="-13.208" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-0.889" x2="-13.208" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-12.065" y1="-0.889" x2="-12.065" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-12.065" y1="0.381" x2="-12.065" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-13.208" y1="0.889" x2="-12.065" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-13.208" y1="0.889" x2="-14.224" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="0.889" x2="-14.224" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="1.651" x2="-13.208" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="1.651" x2="-13.208" y2="1.651" width="0.1524" layer="51"/>
<wire x1="11.049" y1="12.065" x2="10.541" y2="12.065" width="0.1524" layer="21"/>
<wire x1="2.159" y1="12.065" x2="1.651" y2="12.065" width="0.1524" layer="51"/>
<wire x1="1.651" y1="13.208" x2="1.651" y2="12.065" width="0.1524" layer="51"/>
<wire x1="1.651" y1="13.208" x2="1.651" y2="14.224" width="0.1524" layer="21"/>
<wire x1="1.651" y1="14.224" x2="0.889" y2="14.224" width="0.1524" layer="21"/>
<wire x1="0.889" y1="14.224" x2="0.889" y2="13.208" width="0.1524" layer="21"/>
<wire x1="0.889" y1="12.065" x2="0.889" y2="13.208" width="0.1524" layer="51"/>
<wire x1="0.889" y1="12.065" x2="0.381" y2="12.065" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="12.065" x2="-0.889" y2="12.065" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="13.208" x2="-0.889" y2="12.065" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="13.208" x2="-0.889" y2="14.224" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="14.224" x2="-1.651" y2="14.224" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="14.224" x2="-1.651" y2="13.208" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="12.065" x2="-1.651" y2="13.208" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="12.065" x2="-2.159" y2="12.065" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-12.065" x2="2.159" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="2.921" y1="-12.065" x2="3.429" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="3.429" y1="-13.208" x2="3.429" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="3.429" y1="-13.208" x2="3.429" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-14.224" x2="4.191" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="4.191" y1="-14.224" x2="4.191" y2="-13.208" width="0.1524" layer="21"/>
<wire x1="4.191" y1="-12.065" x2="4.191" y2="-13.208" width="0.1524" layer="51"/>
<wire x1="4.191" y1="-12.065" x2="4.699" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="12.065" x2="-3.429" y2="12.065" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="13.208" x2="-3.429" y2="12.065" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="13.208" x2="-3.429" y2="14.224" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="14.224" x2="-4.191" y2="14.224" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="14.224" x2="-4.191" y2="13.208" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="12.065" x2="-4.191" y2="13.208" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="12.065" x2="-4.699" y2="12.065" width="0.1524" layer="51"/>
<wire x1="-12.065" y1="4.699" x2="-12.065" y2="4.191" width="0.1524" layer="51"/>
<wire x1="-13.208" y1="4.191" x2="-12.065" y2="4.191" width="0.1524" layer="51"/>
<wire x1="-13.208" y1="4.191" x2="-14.224" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="4.191" x2="-14.224" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="3.429" x2="-13.208" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="3.429" x2="-13.208" y2="3.429" width="0.1524" layer="51"/>
<wire x1="-12.065" y1="3.429" x2="-12.065" y2="2.921" width="0.1524" layer="51"/>
<wire x1="12.065" y1="-4.699" x2="12.065" y2="-4.191" width="0.1524" layer="51"/>
<wire x1="13.208" y1="-4.191" x2="12.065" y2="-4.191" width="0.1524" layer="51"/>
<wire x1="13.208" y1="-4.191" x2="14.224" y2="-4.191" width="0.1524" layer="21"/>
<wire x1="14.224" y1="-4.191" x2="14.224" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="14.224" y1="-3.429" x2="13.208" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-3.429" x2="13.208" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="12.065" y1="-3.429" x2="12.065" y2="-2.921" width="0.1524" layer="51"/>
<wire x1="10.795" y1="-12.065" x2="12.065" y2="-12.065" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-12.065" x2="12.065" y2="-10.795" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-1.651" x2="12.065" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="-10.795" y1="12.065" x2="-10.541" y2="12.065" width="0.1524" layer="51"/>
<wire x1="-12.065" y1="10.541" x2="-12.065" y2="10.795" width="0.1524" layer="51"/>
<wire x1="-12.065" y1="2.159" x2="-12.065" y2="1.651" width="0.1524" layer="51"/>
<wire x1="13.208" y1="-6.731" x2="12.065" y2="-6.731" width="0.1524" layer="51"/>
<wire x1="13.208" y1="-6.731" x2="14.224" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="14.224" y1="-6.731" x2="14.224" y2="-5.969" width="0.1524" layer="21"/>
<wire x1="14.224" y1="-5.969" x2="13.208" y2="-5.969" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-5.969" x2="13.208" y2="-5.969" width="0.1524" layer="51"/>
<wire x1="12.065" y1="-5.969" x2="12.065" y2="-5.461" width="0.1524" layer="51"/>
<wire x1="12.065" y1="5.461" x2="12.065" y2="5.969" width="0.1524" layer="51"/>
<wire x1="13.208" y1="5.969" x2="12.065" y2="5.969" width="0.1524" layer="51"/>
<wire x1="13.208" y1="5.969" x2="14.224" y2="5.969" width="0.1524" layer="21"/>
<wire x1="14.224" y1="5.969" x2="14.224" y2="6.731" width="0.1524" layer="21"/>
<wire x1="14.224" y1="6.731" x2="13.208" y2="6.731" width="0.1524" layer="21"/>
<wire x1="12.065" y1="6.731" x2="13.208" y2="6.731" width="0.1524" layer="51"/>
<wire x1="6.731" y1="13.208" x2="6.731" y2="12.065" width="0.1524" layer="51"/>
<wire x1="6.731" y1="13.208" x2="6.731" y2="14.224" width="0.1524" layer="21"/>
<wire x1="6.731" y1="14.224" x2="5.969" y2="14.224" width="0.1524" layer="21"/>
<wire x1="5.969" y1="14.224" x2="5.969" y2="13.208" width="0.1524" layer="21"/>
<wire x1="5.969" y1="12.065" x2="5.969" y2="13.208" width="0.1524" layer="51"/>
<wire x1="5.969" y1="12.065" x2="5.461" y2="12.065" width="0.1524" layer="51"/>
<wire x1="-5.461" y1="12.065" x2="-5.969" y2="12.065" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="13.208" x2="-5.969" y2="12.065" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="13.208" x2="-5.969" y2="14.224" width="0.1524" layer="21"/>
<wire x1="-5.969" y1="14.224" x2="-6.731" y2="14.224" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="14.224" x2="-6.731" y2="13.208" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="12.065" x2="-6.731" y2="13.208" width="0.1524" layer="51"/>
<wire x1="-13.208" y1="6.731" x2="-12.065" y2="6.731" width="0.1524" layer="51"/>
<wire x1="-13.208" y1="6.731" x2="-14.224" y2="6.731" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="6.731" x2="-14.224" y2="5.969" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="5.969" x2="-13.208" y2="5.969" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="5.969" x2="-13.208" y2="5.969" width="0.1524" layer="51"/>
<wire x1="-12.065" y1="5.969" x2="-12.065" y2="5.461" width="0.1524" layer="51"/>
<wire x1="5.461" y1="-12.065" x2="5.969" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="5.969" y1="-13.208" x2="5.969" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="5.969" y1="-13.208" x2="5.969" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="5.969" y1="-14.224" x2="6.731" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-14.224" x2="6.731" y2="-13.208" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-12.065" x2="6.731" y2="-13.208" width="0.1524" layer="51"/>
<wire x1="-6.731" y1="-13.208" x2="-6.731" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="-6.731" y1="-13.208" x2="-6.731" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-14.224" x2="-5.969" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="-5.969" y1="-14.224" x2="-5.969" y2="-13.208" width="0.1524" layer="21"/>
<wire x1="-5.969" y1="-12.065" x2="-5.969" y2="-13.208" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="-12.065" x2="-5.461" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="-12.065" y1="-5.461" x2="-12.065" y2="-5.969" width="0.1524" layer="51"/>
<wire x1="-13.208" y1="-5.969" x2="-12.065" y2="-5.969" width="0.1524" layer="51"/>
<wire x1="-13.208" y1="-5.969" x2="-14.224" y2="-5.969" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="-5.969" x2="-14.224" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="-6.731" x2="-13.208" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-6.731" x2="-13.208" y2="-6.731" width="0.1524" layer="51"/>
<wire x1="12.065" y1="8.001" x2="12.065" y2="8.509" width="0.1524" layer="51"/>
<wire x1="13.208" y1="8.509" x2="12.065" y2="8.509" width="0.1524" layer="51"/>
<wire x1="13.208" y1="8.509" x2="14.224" y2="8.509" width="0.1524" layer="21"/>
<wire x1="14.224" y1="8.509" x2="14.224" y2="9.271" width="0.1524" layer="21"/>
<wire x1="14.224" y1="9.271" x2="13.208" y2="9.271" width="0.1524" layer="21"/>
<wire x1="12.065" y1="9.271" x2="13.208" y2="9.271" width="0.1524" layer="51"/>
<wire x1="12.065" y1="9.779" x2="12.065" y2="9.271" width="0.1524" layer="51"/>
<wire x1="12.065" y1="6.731" x2="12.065" y2="7.239" width="0.1524" layer="51"/>
<wire x1="6.731" y1="12.065" x2="7.239" y2="12.065" width="0.1524" layer="51"/>
<wire x1="9.271" y1="13.208" x2="9.271" y2="12.065" width="0.1524" layer="51"/>
<wire x1="9.271" y1="13.208" x2="9.271" y2="14.224" width="0.1524" layer="21"/>
<wire x1="9.271" y1="14.224" x2="8.509" y2="14.224" width="0.1524" layer="21"/>
<wire x1="8.509" y1="14.224" x2="8.509" y2="13.208" width="0.1524" layer="21"/>
<wire x1="8.509" y1="12.065" x2="8.509" y2="13.208" width="0.1524" layer="51"/>
<wire x1="8.001" y1="12.065" x2="8.509" y2="12.065" width="0.1524" layer="51"/>
<wire x1="9.779" y1="12.065" x2="9.271" y2="12.065" width="0.1524" layer="51"/>
<wire x1="-8.001" y1="12.065" x2="-8.509" y2="12.065" width="0.1524" layer="51"/>
<wire x1="-8.509" y1="13.208" x2="-8.509" y2="12.065" width="0.1524" layer="51"/>
<wire x1="-8.509" y1="13.208" x2="-8.509" y2="14.224" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="14.224" x2="-9.271" y2="14.224" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="14.224" x2="-9.271" y2="13.208" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="12.065" x2="-9.271" y2="13.208" width="0.1524" layer="51"/>
<wire x1="-9.271" y1="12.065" x2="-9.779" y2="12.065" width="0.1524" layer="51"/>
<wire x1="-6.731" y1="12.065" x2="-7.239" y2="12.065" width="0.1524" layer="51"/>
<wire x1="12.065" y1="-6.731" x2="12.065" y2="-7.239" width="0.1524" layer="51"/>
<wire x1="12.065" y1="-9.779" x2="12.065" y2="-9.271" width="0.1524" layer="51"/>
<wire x1="13.208" y1="-9.271" x2="12.065" y2="-9.271" width="0.1524" layer="51"/>
<wire x1="13.208" y1="-9.271" x2="14.224" y2="-9.271" width="0.1524" layer="21"/>
<wire x1="14.224" y1="-9.271" x2="14.224" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="14.224" y1="-8.509" x2="13.208" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-8.509" x2="13.208" y2="-8.509" width="0.1524" layer="51"/>
<wire x1="12.065" y1="-8.001" x2="12.065" y2="-8.509" width="0.1524" layer="51"/>
<wire x1="9.779" y1="-12.065" x2="9.271" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="9.271" y1="-13.208" x2="9.271" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="-9.779" y1="-12.065" x2="-9.271" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="-9.271" y1="-13.208" x2="-9.271" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="-9.271" y1="-13.208" x2="-9.271" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="-14.224" x2="-8.509" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="-14.224" x2="-8.509" y2="-13.208" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="-12.065" x2="-8.509" y2="-13.208" width="0.1524" layer="51"/>
<wire x1="-8.001" y1="-12.065" x2="-8.509" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="8.001" y1="-12.065" x2="8.509" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="8.509" y1="-13.208" x2="8.509" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="8.509" y1="-13.208" x2="8.509" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-14.224" x2="9.271" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="9.271" y1="-14.224" x2="9.271" y2="-13.208" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-12.065" x2="7.239" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="-6.731" y1="-12.065" x2="-7.239" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="-12.065" y1="-8.001" x2="-12.065" y2="-8.509" width="0.1524" layer="51"/>
<wire x1="-13.208" y1="-8.509" x2="-12.065" y2="-8.509" width="0.1524" layer="51"/>
<wire x1="-13.208" y1="-8.509" x2="-14.224" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="-8.509" x2="-14.224" y2="-9.271" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="-9.271" x2="-13.208" y2="-9.271" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-9.271" x2="-13.208" y2="-9.271" width="0.1524" layer="51"/>
<wire x1="-12.065" y1="-9.271" x2="-12.065" y2="-9.779" width="0.1524" layer="51"/>
<wire x1="-12.065" y1="-6.731" x2="-12.065" y2="-7.239" width="0.1524" layer="51"/>
<wire x1="-12.065" y1="6.731" x2="-12.065" y2="7.239" width="0.1524" layer="51"/>
<wire x1="-12.065" y1="9.779" x2="-12.065" y2="9.271" width="0.1524" layer="51"/>
<wire x1="-13.208" y1="9.271" x2="-12.065" y2="9.271" width="0.1524" layer="51"/>
<wire x1="-13.208" y1="9.271" x2="-14.224" y2="9.271" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="9.271" x2="-14.224" y2="8.509" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="8.509" x2="-13.208" y2="8.509" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="8.509" x2="-13.208" y2="8.509" width="0.1524" layer="51"/>
<wire x1="-12.065" y1="8.001" x2="-12.065" y2="8.509" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-7.62" x2="-7.62" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-5.715" x2="-5.715" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-7.62" x2="7.62" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-5.715" x2="5.715" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="5.715" x2="-7.62" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="7.62" x2="-5.715" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="7.62" x2="-7.62" y2="5.715" width="0.1524" layer="21"/>
<wire x1="5.715" y1="7.62" x2="7.62" y2="7.62" width="0.1524" layer="21"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="5.715" width="0.1524" layer="21"/>
<wire x1="7.62" y1="5.715" x2="5.715" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-10.541" y1="-13.716" x2="-10.541" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="-10.541" y1="-12.065" x2="-10.795" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="-10.541" y1="-13.716" x2="-10.541" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="-10.541" y1="-14.224" x2="-9.779" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-14.224" x2="-9.779" y2="-13.716" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-12.065" x2="-9.779" y2="-13.716" width="0.1524" layer="51"/>
<wire x1="-5.461" y1="-13.716" x2="-5.461" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="-5.461" y1="-13.716" x2="-5.461" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="-5.461" y1="-14.224" x2="-4.699" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-14.224" x2="-4.699" y2="-13.716" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-12.065" x2="-4.699" y2="-13.716" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="-13.716" x2="-2.921" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="-13.716" x2="-2.921" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-14.224" x2="-2.159" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="-14.224" x2="-2.159" y2="-13.716" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="-12.065" x2="-2.159" y2="-13.716" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="-13.716" x2="-0.381" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="-13.716" x2="-0.381" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="-14.224" x2="0.381" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="0.381" y1="-14.224" x2="0.381" y2="-13.716" width="0.1524" layer="21"/>
<wire x1="0.381" y1="-12.065" x2="0.381" y2="-13.716" width="0.1524" layer="51"/>
<wire x1="13.716" y1="10.541" x2="12.065" y2="10.541" width="0.1524" layer="51"/>
<wire x1="12.065" y1="10.541" x2="12.065" y2="10.795" width="0.1524" layer="51"/>
<wire x1="13.716" y1="10.541" x2="14.224" y2="10.541" width="0.1524" layer="21"/>
<wire x1="14.224" y1="10.541" x2="14.224" y2="9.779" width="0.1524" layer="21"/>
<wire x1="14.224" y1="9.779" x2="13.716" y2="9.779" width="0.1524" layer="21"/>
<wire x1="12.065" y1="9.779" x2="13.716" y2="9.779" width="0.1524" layer="51"/>
<wire x1="13.716" y1="5.461" x2="12.065" y2="5.461" width="0.1524" layer="51"/>
<wire x1="13.716" y1="5.461" x2="14.224" y2="5.461" width="0.1524" layer="21"/>
<wire x1="14.224" y1="5.461" x2="14.224" y2="4.699" width="0.1524" layer="21"/>
<wire x1="14.224" y1="4.699" x2="13.716" y2="4.699" width="0.1524" layer="21"/>
<wire x1="12.065" y1="4.699" x2="13.716" y2="4.699" width="0.1524" layer="51"/>
<wire x1="13.716" y1="2.921" x2="12.065" y2="2.921" width="0.1524" layer="51"/>
<wire x1="13.716" y1="2.921" x2="14.224" y2="2.921" width="0.1524" layer="21"/>
<wire x1="14.224" y1="2.921" x2="14.224" y2="2.159" width="0.1524" layer="21"/>
<wire x1="14.224" y1="2.159" x2="13.716" y2="2.159" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.159" x2="13.716" y2="2.159" width="0.1524" layer="51"/>
<wire x1="13.716" y1="0.381" x2="12.065" y2="0.381" width="0.1524" layer="51"/>
<wire x1="13.716" y1="0.381" x2="14.224" y2="0.381" width="0.1524" layer="21"/>
<wire x1="14.224" y1="0.381" x2="14.224" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="14.224" y1="-0.381" x2="13.716" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-0.381" x2="13.716" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-13.716" y1="-10.541" x2="-12.065" y2="-10.541" width="0.1524" layer="51"/>
<wire x1="-12.065" y1="-10.541" x2="-12.065" y2="-10.795" width="0.1524" layer="51"/>
<wire x1="-13.716" y1="-10.541" x2="-14.224" y2="-10.541" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="-10.541" x2="-14.224" y2="-9.779" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="-9.779" x2="-13.716" y2="-9.779" width="0.1524" layer="21"/>
<wire x1="-13.716" y1="-5.461" x2="-12.065" y2="-5.461" width="0.1524" layer="51"/>
<wire x1="-13.716" y1="-5.461" x2="-14.224" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="-5.461" x2="-14.224" y2="-4.699" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="-4.699" x2="-13.716" y2="-4.699" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-4.699" x2="-13.716" y2="-4.699" width="0.1524" layer="51"/>
<wire x1="-13.716" y1="-2.921" x2="-12.065" y2="-2.921" width="0.1524" layer="51"/>
<wire x1="-13.716" y1="-2.921" x2="-14.224" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="-2.921" x2="-14.224" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="-2.159" x2="-13.716" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-2.159" x2="-13.716" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="-13.716" y1="-0.381" x2="-12.065" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-13.716" y1="-0.381" x2="-14.224" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="-0.381" x2="-14.224" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="0.381" x2="-13.716" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="0.381" x2="-13.716" y2="0.381" width="0.1524" layer="51"/>
<wire x1="10.541" y1="13.716" x2="10.541" y2="12.065" width="0.1524" layer="51"/>
<wire x1="10.541" y1="13.716" x2="10.541" y2="14.224" width="0.1524" layer="21"/>
<wire x1="10.541" y1="14.224" x2="9.779" y2="14.224" width="0.1524" layer="21"/>
<wire x1="9.779" y1="14.224" x2="9.779" y2="13.716" width="0.1524" layer="21"/>
<wire x1="9.779" y1="12.065" x2="9.779" y2="13.716" width="0.1524" layer="51"/>
<wire x1="5.461" y1="13.716" x2="5.461" y2="12.065" width="0.1524" layer="51"/>
<wire x1="5.461" y1="13.716" x2="5.461" y2="14.224" width="0.1524" layer="21"/>
<wire x1="5.461" y1="14.224" x2="4.699" y2="14.224" width="0.1524" layer="21"/>
<wire x1="4.699" y1="14.224" x2="4.699" y2="13.716" width="0.1524" layer="21"/>
<wire x1="4.699" y1="12.065" x2="4.699" y2="13.716" width="0.1524" layer="51"/>
<wire x1="2.921" y1="13.716" x2="2.921" y2="12.065" width="0.1524" layer="51"/>
<wire x1="2.921" y1="13.716" x2="2.921" y2="14.224" width="0.1524" layer="21"/>
<wire x1="2.921" y1="14.224" x2="2.159" y2="14.224" width="0.1524" layer="21"/>
<wire x1="2.159" y1="14.224" x2="2.159" y2="13.716" width="0.1524" layer="21"/>
<wire x1="2.159" y1="12.065" x2="2.159" y2="13.716" width="0.1524" layer="51"/>
<wire x1="0.381" y1="13.716" x2="0.381" y2="12.065" width="0.1524" layer="51"/>
<wire x1="0.381" y1="13.716" x2="0.381" y2="14.224" width="0.1524" layer="21"/>
<wire x1="0.381" y1="14.224" x2="-0.381" y2="14.224" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="14.224" x2="-0.381" y2="13.716" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="12.065" x2="-0.381" y2="13.716" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="13.716" x2="-2.159" y2="12.065" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="13.716" x2="-2.159" y2="14.224" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="14.224" x2="-2.921" y2="14.224" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="14.224" x2="-2.921" y2="13.716" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="12.065" x2="-2.921" y2="13.716" width="0.1524" layer="51"/>
<wire x1="-4.699" y1="13.716" x2="-4.699" y2="12.065" width="0.1524" layer="51"/>
<wire x1="-4.699" y1="13.716" x2="-4.699" y2="14.224" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="14.224" x2="-5.461" y2="14.224" width="0.1524" layer="21"/>
<wire x1="-5.461" y1="14.224" x2="-5.461" y2="13.716" width="0.1524" layer="21"/>
<wire x1="-5.461" y1="12.065" x2="-5.461" y2="13.716" width="0.1524" layer="51"/>
<wire x1="-9.779" y1="13.716" x2="-9.779" y2="12.065" width="0.1524" layer="51"/>
<wire x1="-9.779" y1="13.716" x2="-9.779" y2="14.224" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="14.224" x2="-10.541" y2="14.224" width="0.1524" layer="21"/>
<wire x1="-10.541" y1="14.224" x2="-10.541" y2="13.716" width="0.1524" layer="21"/>
<wire x1="-10.541" y1="12.065" x2="-10.541" y2="13.716" width="0.1524" layer="51"/>
<wire x1="2.159" y1="-13.716" x2="2.159" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="2.159" y1="-13.716" x2="2.159" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-14.224" x2="2.921" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-14.224" x2="2.921" y2="-13.716" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-12.065" x2="2.921" y2="-13.716" width="0.1524" layer="51"/>
<wire x1="4.699" y1="-13.716" x2="4.699" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="4.699" y1="-13.716" x2="4.699" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-14.224" x2="5.461" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="5.461" y1="-14.224" x2="5.461" y2="-13.716" width="0.1524" layer="21"/>
<wire x1="5.461" y1="-12.065" x2="5.461" y2="-13.716" width="0.1524" layer="51"/>
<wire x1="9.779" y1="-13.716" x2="9.779" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="9.779" y1="-13.716" x2="9.779" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-14.224" x2="10.541" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="10.541" y1="-14.224" x2="10.541" y2="-13.716" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-12.065" x2="10.541" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="10.541" y1="-12.065" x2="10.541" y2="-13.716" width="0.1524" layer="51"/>
<wire x1="-13.716" y1="10.541" x2="-12.065" y2="10.541" width="0.1524" layer="51"/>
<wire x1="-13.716" y1="10.541" x2="-14.224" y2="10.541" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="10.541" x2="-14.224" y2="9.779" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="9.779" x2="-13.716" y2="9.779" width="0.1524" layer="21"/>
<wire x1="-13.716" y1="5.461" x2="-12.065" y2="5.461" width="0.1524" layer="51"/>
<wire x1="-13.716" y1="5.461" x2="-14.224" y2="5.461" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="5.461" x2="-14.224" y2="4.699" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="4.699" x2="-13.716" y2="4.699" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="4.699" x2="-13.716" y2="4.699" width="0.1524" layer="51"/>
<wire x1="-13.716" y1="2.921" x2="-12.065" y2="2.921" width="0.1524" layer="51"/>
<wire x1="-13.716" y1="2.921" x2="-14.224" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="2.921" x2="-14.224" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="2.159" x2="-13.716" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="2.159" x2="-13.716" y2="2.159" width="0.1524" layer="51"/>
<wire x1="13.716" y1="-10.541" x2="12.065" y2="-10.541" width="0.1524" layer="51"/>
<wire x1="12.065" y1="-10.541" x2="12.065" y2="-10.795" width="0.1524" layer="51"/>
<wire x1="13.716" y1="-10.541" x2="14.224" y2="-10.541" width="0.1524" layer="21"/>
<wire x1="14.224" y1="-10.541" x2="14.224" y2="-9.779" width="0.1524" layer="21"/>
<wire x1="14.224" y1="-9.779" x2="13.716" y2="-9.779" width="0.1524" layer="21"/>
<wire x1="13.716" y1="-5.461" x2="12.065" y2="-5.461" width="0.1524" layer="51"/>
<wire x1="13.716" y1="-5.461" x2="14.224" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="14.224" y1="-5.461" x2="14.224" y2="-4.699" width="0.1524" layer="21"/>
<wire x1="14.224" y1="-4.699" x2="13.716" y2="-4.699" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-4.699" x2="13.716" y2="-4.699" width="0.1524" layer="51"/>
<wire x1="13.716" y1="-2.921" x2="12.065" y2="-2.921" width="0.1524" layer="51"/>
<wire x1="13.716" y1="-2.921" x2="14.224" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="14.224" y1="-2.921" x2="14.224" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="14.224" y1="-2.159" x2="13.716" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-2.159" x2="13.716" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="13.716" y1="7.239" x2="12.065" y2="7.239" width="0.1524" layer="51"/>
<wire x1="13.716" y1="7.239" x2="14.224" y2="7.239" width="0.1524" layer="21"/>
<wire x1="14.224" y1="7.239" x2="14.224" y2="8.001" width="0.1524" layer="21"/>
<wire x1="14.224" y1="8.001" x2="13.716" y2="8.001" width="0.1524" layer="21"/>
<wire x1="12.065" y1="8.001" x2="13.716" y2="8.001" width="0.1524" layer="51"/>
<wire x1="7.239" y1="13.716" x2="7.239" y2="12.065" width="0.1524" layer="51"/>
<wire x1="7.239" y1="13.716" x2="7.239" y2="14.224" width="0.1524" layer="21"/>
<wire x1="7.239" y1="14.224" x2="8.001" y2="14.224" width="0.1524" layer="21"/>
<wire x1="8.001" y1="14.224" x2="8.001" y2="13.716" width="0.1524" layer="21"/>
<wire x1="8.001" y1="12.065" x2="8.001" y2="13.716" width="0.1524" layer="51"/>
<wire x1="-7.239" y1="13.716" x2="-7.239" y2="12.065" width="0.1524" layer="51"/>
<wire x1="-7.239" y1="13.716" x2="-7.239" y2="14.224" width="0.1524" layer="21"/>
<wire x1="-7.239" y1="14.224" x2="-8.001" y2="14.224" width="0.1524" layer="21"/>
<wire x1="-8.001" y1="14.224" x2="-8.001" y2="13.716" width="0.1524" layer="21"/>
<wire x1="-8.001" y1="12.065" x2="-8.001" y2="13.716" width="0.1524" layer="51"/>
<wire x1="13.716" y1="-7.239" x2="12.065" y2="-7.239" width="0.1524" layer="51"/>
<wire x1="13.716" y1="-7.239" x2="14.224" y2="-7.239" width="0.1524" layer="21"/>
<wire x1="14.224" y1="-7.239" x2="14.224" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="14.224" y1="-8.001" x2="13.716" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-8.001" x2="13.716" y2="-8.001" width="0.1524" layer="51"/>
<wire x1="12.065" y1="-9.779" x2="13.716" y2="-9.779" width="0.1524" layer="51"/>
<wire x1="7.239" y1="-13.716" x2="7.239" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="7.239" y1="-13.716" x2="7.239" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="7.239" y1="-14.224" x2="8.001" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="8.001" y1="-14.224" x2="8.001" y2="-13.716" width="0.1524" layer="21"/>
<wire x1="8.001" y1="-12.065" x2="8.001" y2="-13.716" width="0.1524" layer="51"/>
<wire x1="-7.239" y1="-13.716" x2="-7.239" y2="-12.065" width="0.1524" layer="51"/>
<wire x1="-7.239" y1="-13.716" x2="-7.239" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="-7.239" y1="-14.224" x2="-8.001" y2="-14.224" width="0.1524" layer="21"/>
<wire x1="-8.001" y1="-14.224" x2="-8.001" y2="-13.716" width="0.1524" layer="21"/>
<wire x1="-8.001" y1="-12.065" x2="-8.001" y2="-13.716" width="0.1524" layer="51"/>
<wire x1="-13.716" y1="-7.239" x2="-12.065" y2="-7.239" width="0.1524" layer="51"/>
<wire x1="-13.716" y1="-7.239" x2="-14.224" y2="-7.239" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="-7.239" x2="-14.224" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="-8.001" x2="-13.716" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-8.001" x2="-13.716" y2="-8.001" width="0.1524" layer="51"/>
<wire x1="-13.716" y1="-9.779" x2="-12.065" y2="-9.779" width="0.1524" layer="51"/>
<wire x1="-13.716" y1="7.239" x2="-12.065" y2="7.239" width="0.1524" layer="51"/>
<wire x1="-13.716" y1="7.239" x2="-14.224" y2="7.239" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="7.239" x2="-14.224" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-14.224" y1="8.001" x2="-13.716" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="8.001" x2="-13.716" y2="8.001" width="0.1524" layer="51"/>
<wire x1="-12.065" y1="9.779" x2="-13.716" y2="9.779" width="0.1524" layer="51"/>
<wire x1="-15.494" y1="-14.859" x2="-14.859" y2="-15.494" width="0.1524" layer="21" curve="90"/>
<wire x1="14.859" y1="-15.494" x2="-14.859" y2="-15.494" width="0.1524" layer="21"/>
<wire x1="14.859" y1="-15.494" x2="15.494" y2="-14.859" width="0.1524" layer="21" curve="90"/>
<wire x1="14.859" y1="15.494" x2="-13.97" y2="15.494" width="0.1524" layer="21"/>
<wire x1="15.494" y1="14.859" x2="15.494" y2="-14.859" width="0.1524" layer="21"/>
<wire x1="14.859" y1="15.494" x2="15.494" y2="14.859" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="6.35" x2="1.27" y2="6.35" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.35" x2="0" y2="7.62" width="0.1524" layer="21"/>
<wire x1="0" y1="7.62" x2="-1.27" y2="6.35" width="0.1524" layer="21"/>
<circle x="3.81" y="3.81" radius="2.54" width="0.1524" layer="21"/>
<circle x="-3.81" y="3.81" radius="2.54" width="0.1524" layer="21"/>
<circle x="-3.81" y="-3.81" radius="2.54" width="0.1524" layer="21"/>
<circle x="3.81" y="-3.81" radius="2.54" width="0.1524" layer="21"/>
<pad name="1" x="0" y="12.7" drill="0.8128"/>
<pad name="2" x="0" y="10.16" drill="0.8128" shape="octagon"/>
<pad name="3" x="-2.54" y="12.7" drill="0.8128" shape="octagon"/>
<pad name="4" x="-2.54" y="10.16" drill="0.8128" shape="octagon"/>
<pad name="5" x="-5.08" y="12.7" drill="0.8128" shape="octagon"/>
<pad name="6" x="-5.08" y="10.16" drill="0.8128" shape="octagon"/>
<pad name="7" x="-7.62" y="12.7" drill="0.8128" shape="octagon"/>
<pad name="8" x="-7.62" y="10.16" drill="0.8128" shape="octagon"/>
<pad name="9" x="-10.16" y="12.7" drill="0.8128" shape="octagon"/>
<pad name="10" x="-12.7" y="10.16" drill="0.8128" shape="octagon"/>
<pad name="11" x="-10.16" y="10.16" drill="0.8128" shape="octagon"/>
<pad name="12" x="-12.7" y="7.62" drill="0.8128" shape="octagon"/>
<pad name="13" x="-10.16" y="7.62" drill="0.8128" shape="octagon"/>
<pad name="14" x="-12.7" y="5.08" drill="0.8128" shape="octagon"/>
<pad name="15" x="-10.16" y="5.08" drill="0.8128" shape="octagon"/>
<pad name="16" x="-12.7" y="2.54" drill="0.8128" shape="octagon"/>
<pad name="17" x="-10.16" y="2.54" drill="0.8128" shape="octagon"/>
<pad name="18" x="-12.7" y="0" drill="0.8128" shape="octagon"/>
<pad name="19" x="-10.16" y="0" drill="0.8128" shape="octagon"/>
<pad name="20" x="-12.7" y="-2.54" drill="0.8128" shape="octagon"/>
<pad name="21" x="-10.16" y="-2.54" drill="0.8128" shape="octagon"/>
<pad name="22" x="-12.7" y="-5.08" drill="0.8128" shape="octagon"/>
<pad name="23" x="-10.16" y="-5.08" drill="0.8128" shape="octagon"/>
<pad name="24" x="-12.7" y="-7.62" drill="0.8128" shape="octagon"/>
<pad name="25" x="-10.16" y="-7.62" drill="0.8128" shape="octagon"/>
<pad name="26" x="-12.7" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="27" x="-10.16" y="-12.7" drill="0.8128" shape="octagon"/>
<pad name="28" x="-10.16" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="29" x="-7.62" y="-12.7" drill="0.8128" shape="octagon"/>
<pad name="30" x="-7.62" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="31" x="-5.08" y="-12.7" drill="0.8128" shape="octagon"/>
<pad name="32" x="-5.08" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="33" x="-2.54" y="-12.7" drill="0.8128" shape="octagon"/>
<pad name="34" x="-2.54" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="35" x="0" y="-12.7" drill="0.8128" shape="octagon"/>
<pad name="36" x="0" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="37" x="2.54" y="-12.7" drill="0.8128" shape="octagon"/>
<pad name="38" x="2.54" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="39" x="5.08" y="-12.7" drill="0.8128" shape="octagon"/>
<pad name="40" x="5.08" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="41" x="7.62" y="-12.7" drill="0.8128" shape="octagon"/>
<pad name="42" x="7.62" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="43" x="10.16" y="-12.7" drill="0.8128" shape="octagon"/>
<pad name="44" x="12.7" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="45" x="10.16" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="46" x="12.7" y="-7.62" drill="0.8128" shape="octagon"/>
<pad name="47" x="10.16" y="-7.62" drill="0.8128" shape="octagon"/>
<pad name="48" x="12.7" y="-5.08" drill="0.8128" shape="octagon"/>
<pad name="49" x="10.16" y="-5.08" drill="0.8128" shape="octagon"/>
<pad name="50" x="12.7" y="-2.54" drill="0.8128" shape="octagon"/>
<pad name="51" x="10.16" y="-2.54" drill="0.8128" shape="octagon"/>
<pad name="52" x="12.7" y="0" drill="0.8128" shape="octagon"/>
<pad name="53" x="10.16" y="0" drill="0.8128" shape="octagon"/>
<pad name="54" x="12.7" y="2.54" drill="0.8128" shape="octagon"/>
<pad name="55" x="10.16" y="2.54" drill="0.8128" shape="octagon"/>
<pad name="56" x="12.7" y="5.08" drill="0.8128" shape="octagon"/>
<pad name="57" x="10.16" y="5.08" drill="0.8128" shape="octagon"/>
<pad name="58" x="12.7" y="7.62" drill="0.8128" shape="octagon"/>
<pad name="59" x="10.16" y="7.62" drill="0.8128" shape="octagon"/>
<pad name="60" x="12.7" y="10.16" drill="0.8128" shape="octagon"/>
<pad name="61" x="10.16" y="12.7" drill="0.8128" shape="octagon"/>
<pad name="62" x="10.16" y="10.16" drill="0.8128" shape="octagon"/>
<pad name="63" x="7.62" y="12.7" drill="0.8128" shape="octagon"/>
<pad name="64" x="7.62" y="10.16" drill="0.8128" shape="octagon"/>
<pad name="65" x="5.08" y="12.7" drill="0.8128" shape="octagon"/>
<pad name="66" x="5.08" y="10.16" drill="0.8128" shape="octagon"/>
<pad name="67" x="2.54" y="12.7" drill="0.8128" shape="octagon"/>
<pad name="68" x="2.54" y="10.16" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="15.875" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-13.843" y="15.875" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="12.065" y="-14.605" size="1.27" layer="21" ratio="10">68</text>
<rectangle x1="-0.508" y1="4.699" x2="0.508" y2="6.604" layer="21"/>
<rectangle x1="-0.889" y1="6.35" x2="0.889" y2="6.731" layer="21"/>
<rectangle x1="-0.635" y1="6.731" x2="0.635" y2="6.985" layer="21"/>
<rectangle x1="-0.381" y1="6.985" x2="0.381" y2="7.239" layer="21"/>
<rectangle x1="-0.127" y1="7.239" x2="0.127" y2="7.493" layer="21"/>
<rectangle x1="-1.143" y1="6.35" x2="-0.889" y2="6.477" layer="21"/>
<rectangle x1="-1.016" y1="6.477" x2="-0.889" y2="6.604" layer="21"/>
<rectangle x1="-0.762" y1="6.731" x2="-0.635" y2="6.858" layer="21"/>
<rectangle x1="-0.508" y1="6.985" x2="-0.381" y2="7.112" layer="21"/>
<rectangle x1="-0.254" y1="7.239" x2="-0.127" y2="7.366" layer="21"/>
<rectangle x1="0.127" y1="7.239" x2="0.254" y2="7.366" layer="21"/>
<rectangle x1="0.381" y1="6.985" x2="0.508" y2="7.112" layer="21"/>
<rectangle x1="0.635" y1="6.731" x2="0.762" y2="6.858" layer="21"/>
<rectangle x1="0.889" y1="6.35" x2="1.143" y2="6.477" layer="21"/>
<rectangle x1="0.889" y1="6.477" x2="1.016" y2="6.604" layer="21"/>
</package>
<package name="PLCC68-S_TOPMOUNT">
<wire x1="-13.97" y1="15.494" x2="-15.494" y2="13.97" width="0.1524" layer="22"/>
<wire x1="-15.494" y1="13.97" x2="-15.494" y2="-14.859" width="0.1524" layer="22"/>
<wire x1="-12.065" y1="10.795" x2="-10.795" y2="12.065" width="0.1524" layer="22"/>
<wire x1="-13.208" y1="-14.224" x2="-11.049" y2="-12.065" width="0.1524" layer="22"/>
<wire x1="-13.208" y1="-14.224" x2="-14.224" y2="-13.208" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="-13.208" x2="-12.065" y2="-11.049" width="0.1524" layer="22"/>
<wire x1="-11.049" y1="-12.065" x2="-10.795" y2="-12.065" width="0.1524" layer="22"/>
<wire x1="-4.699" y1="-12.065" x2="-4.191" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="-4.191" y1="-13.208" x2="-4.191" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="-4.191" y1="-13.208" x2="-4.191" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="-4.191" y1="-14.224" x2="-3.429" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="-3.429" y1="-14.224" x2="-3.429" y2="-13.208" width="0.1524" layer="22"/>
<wire x1="-3.429" y1="-12.065" x2="-3.429" y2="-13.208" width="0.1524" layer="52"/>
<wire x1="-3.429" y1="-12.065" x2="-2.921" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="-2.159" y1="-12.065" x2="-1.651" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="-1.651" y1="-13.208" x2="-1.651" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="-1.651" y1="-13.208" x2="-1.651" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="-1.651" y1="-14.224" x2="-0.889" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="-0.889" y1="-14.224" x2="-0.889" y2="-13.208" width="0.1524" layer="22"/>
<wire x1="-0.889" y1="-12.065" x2="-0.889" y2="-13.208" width="0.1524" layer="52"/>
<wire x1="-0.889" y1="-12.065" x2="-0.381" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="0.381" y1="-12.065" x2="0.889" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="0.889" y1="-13.208" x2="0.889" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="0.889" y1="-13.208" x2="0.889" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="0.889" y1="-14.224" x2="1.651" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="1.651" y1="-14.224" x2="1.651" y2="-13.208" width="0.1524" layer="22"/>
<wire x1="1.651" y1="-12.065" x2="1.651" y2="-13.208" width="0.1524" layer="52"/>
<wire x1="14.224" y1="13.208" x2="12.065" y2="11.049" width="0.1524" layer="22"/>
<wire x1="14.224" y1="13.208" x2="13.208" y2="14.224" width="0.1524" layer="22"/>
<wire x1="13.208" y1="14.224" x2="11.049" y2="12.065" width="0.1524" layer="22"/>
<wire x1="12.065" y1="11.049" x2="12.065" y2="10.795" width="0.1524" layer="22"/>
<wire x1="12.065" y1="4.699" x2="12.065" y2="4.191" width="0.1524" layer="52"/>
<wire x1="13.208" y1="4.191" x2="12.065" y2="4.191" width="0.1524" layer="52"/>
<wire x1="13.208" y1="4.191" x2="14.224" y2="4.191" width="0.1524" layer="22"/>
<wire x1="14.224" y1="4.191" x2="14.224" y2="3.429" width="0.1524" layer="22"/>
<wire x1="14.224" y1="3.429" x2="13.208" y2="3.429" width="0.1524" layer="22"/>
<wire x1="12.065" y1="3.429" x2="13.208" y2="3.429" width="0.1524" layer="52"/>
<wire x1="12.065" y1="3.429" x2="12.065" y2="2.921" width="0.1524" layer="52"/>
<wire x1="12.065" y1="-0.381" x2="12.065" y2="-0.889" width="0.1524" layer="52"/>
<wire x1="13.208" y1="-0.889" x2="12.065" y2="-0.889" width="0.1524" layer="52"/>
<wire x1="13.208" y1="-0.889" x2="14.224" y2="-0.889" width="0.1524" layer="22"/>
<wire x1="14.224" y1="-0.889" x2="14.224" y2="-1.651" width="0.1524" layer="22"/>
<wire x1="14.224" y1="-1.651" x2="13.208" y2="-1.651" width="0.1524" layer="22"/>
<wire x1="12.065" y1="-1.651" x2="13.208" y2="-1.651" width="0.1524" layer="52"/>
<wire x1="-12.065" y1="-11.049" x2="-12.065" y2="-10.795" width="0.1524" layer="22"/>
<wire x1="4.699" y1="12.065" x2="4.191" y2="12.065" width="0.1524" layer="52"/>
<wire x1="4.191" y1="13.208" x2="4.191" y2="12.065" width="0.1524" layer="52"/>
<wire x1="4.191" y1="13.208" x2="4.191" y2="14.224" width="0.1524" layer="22"/>
<wire x1="4.191" y1="14.224" x2="3.429" y2="14.224" width="0.1524" layer="22"/>
<wire x1="3.429" y1="14.224" x2="3.429" y2="13.208" width="0.1524" layer="22"/>
<wire x1="3.429" y1="12.065" x2="3.429" y2="13.208" width="0.1524" layer="52"/>
<wire x1="3.429" y1="12.065" x2="2.921" y2="12.065" width="0.1524" layer="52"/>
<wire x1="12.065" y1="2.159" x2="12.065" y2="1.651" width="0.1524" layer="52"/>
<wire x1="13.208" y1="1.651" x2="12.065" y2="1.651" width="0.1524" layer="52"/>
<wire x1="13.208" y1="1.651" x2="14.224" y2="1.651" width="0.1524" layer="22"/>
<wire x1="14.224" y1="1.651" x2="14.224" y2="0.889" width="0.1524" layer="22"/>
<wire x1="14.224" y1="0.889" x2="13.208" y2="0.889" width="0.1524" layer="22"/>
<wire x1="12.065" y1="0.889" x2="13.208" y2="0.889" width="0.1524" layer="52"/>
<wire x1="12.065" y1="0.889" x2="12.065" y2="0.381" width="0.1524" layer="52"/>
<wire x1="-12.065" y1="-4.699" x2="-12.065" y2="-4.191" width="0.1524" layer="52"/>
<wire x1="-13.208" y1="-4.191" x2="-12.065" y2="-4.191" width="0.1524" layer="52"/>
<wire x1="-13.208" y1="-4.191" x2="-14.224" y2="-4.191" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="-4.191" x2="-14.224" y2="-3.429" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="-3.429" x2="-13.208" y2="-3.429" width="0.1524" layer="22"/>
<wire x1="-12.065" y1="-3.429" x2="-13.208" y2="-3.429" width="0.1524" layer="52"/>
<wire x1="-12.065" y1="-3.429" x2="-12.065" y2="-2.921" width="0.1524" layer="52"/>
<wire x1="-12.065" y1="-2.159" x2="-12.065" y2="-1.651" width="0.1524" layer="52"/>
<wire x1="-13.208" y1="-1.651" x2="-12.065" y2="-1.651" width="0.1524" layer="52"/>
<wire x1="-13.208" y1="-1.651" x2="-14.224" y2="-1.651" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="-1.651" x2="-14.224" y2="-0.889" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="-0.889" x2="-13.208" y2="-0.889" width="0.1524" layer="22"/>
<wire x1="-12.065" y1="-0.889" x2="-13.208" y2="-0.889" width="0.1524" layer="52"/>
<wire x1="-12.065" y1="-0.889" x2="-12.065" y2="-0.381" width="0.1524" layer="52"/>
<wire x1="-12.065" y1="0.381" x2="-12.065" y2="0.889" width="0.1524" layer="52"/>
<wire x1="-13.208" y1="0.889" x2="-12.065" y2="0.889" width="0.1524" layer="52"/>
<wire x1="-13.208" y1="0.889" x2="-14.224" y2="0.889" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="0.889" x2="-14.224" y2="1.651" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="1.651" x2="-13.208" y2="1.651" width="0.1524" layer="22"/>
<wire x1="-12.065" y1="1.651" x2="-13.208" y2="1.651" width="0.1524" layer="52"/>
<wire x1="11.049" y1="12.065" x2="10.541" y2="12.065" width="0.1524" layer="22"/>
<wire x1="2.159" y1="12.065" x2="1.651" y2="12.065" width="0.1524" layer="52"/>
<wire x1="1.651" y1="13.208" x2="1.651" y2="12.065" width="0.1524" layer="52"/>
<wire x1="1.651" y1="13.208" x2="1.651" y2="14.224" width="0.1524" layer="22"/>
<wire x1="1.651" y1="14.224" x2="0.889" y2="14.224" width="0.1524" layer="22"/>
<wire x1="0.889" y1="14.224" x2="0.889" y2="13.208" width="0.1524" layer="22"/>
<wire x1="0.889" y1="12.065" x2="0.889" y2="13.208" width="0.1524" layer="52"/>
<wire x1="0.889" y1="12.065" x2="0.381" y2="12.065" width="0.1524" layer="52"/>
<wire x1="-0.381" y1="12.065" x2="-0.889" y2="12.065" width="0.1524" layer="52"/>
<wire x1="-0.889" y1="13.208" x2="-0.889" y2="12.065" width="0.1524" layer="52"/>
<wire x1="-0.889" y1="13.208" x2="-0.889" y2="14.224" width="0.1524" layer="22"/>
<wire x1="-0.889" y1="14.224" x2="-1.651" y2="14.224" width="0.1524" layer="22"/>
<wire x1="-1.651" y1="14.224" x2="-1.651" y2="13.208" width="0.1524" layer="22"/>
<wire x1="-1.651" y1="12.065" x2="-1.651" y2="13.208" width="0.1524" layer="52"/>
<wire x1="-1.651" y1="12.065" x2="-2.159" y2="12.065" width="0.1524" layer="52"/>
<wire x1="1.651" y1="-12.065" x2="2.159" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="2.921" y1="-12.065" x2="3.429" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="3.429" y1="-13.208" x2="3.429" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="3.429" y1="-13.208" x2="3.429" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="3.429" y1="-14.224" x2="4.191" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="4.191" y1="-14.224" x2="4.191" y2="-13.208" width="0.1524" layer="22"/>
<wire x1="4.191" y1="-12.065" x2="4.191" y2="-13.208" width="0.1524" layer="52"/>
<wire x1="4.191" y1="-12.065" x2="4.699" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="-2.921" y1="12.065" x2="-3.429" y2="12.065" width="0.1524" layer="52"/>
<wire x1="-3.429" y1="13.208" x2="-3.429" y2="12.065" width="0.1524" layer="52"/>
<wire x1="-3.429" y1="13.208" x2="-3.429" y2="14.224" width="0.1524" layer="22"/>
<wire x1="-3.429" y1="14.224" x2="-4.191" y2="14.224" width="0.1524" layer="22"/>
<wire x1="-4.191" y1="14.224" x2="-4.191" y2="13.208" width="0.1524" layer="22"/>
<wire x1="-4.191" y1="12.065" x2="-4.191" y2="13.208" width="0.1524" layer="52"/>
<wire x1="-4.191" y1="12.065" x2="-4.699" y2="12.065" width="0.1524" layer="52"/>
<wire x1="-12.065" y1="4.699" x2="-12.065" y2="4.191" width="0.1524" layer="52"/>
<wire x1="-13.208" y1="4.191" x2="-12.065" y2="4.191" width="0.1524" layer="52"/>
<wire x1="-13.208" y1="4.191" x2="-14.224" y2="4.191" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="4.191" x2="-14.224" y2="3.429" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="3.429" x2="-13.208" y2="3.429" width="0.1524" layer="22"/>
<wire x1="-12.065" y1="3.429" x2="-13.208" y2="3.429" width="0.1524" layer="52"/>
<wire x1="-12.065" y1="3.429" x2="-12.065" y2="2.921" width="0.1524" layer="52"/>
<wire x1="12.065" y1="-4.699" x2="12.065" y2="-4.191" width="0.1524" layer="52"/>
<wire x1="13.208" y1="-4.191" x2="12.065" y2="-4.191" width="0.1524" layer="52"/>
<wire x1="13.208" y1="-4.191" x2="14.224" y2="-4.191" width="0.1524" layer="22"/>
<wire x1="14.224" y1="-4.191" x2="14.224" y2="-3.429" width="0.1524" layer="22"/>
<wire x1="14.224" y1="-3.429" x2="13.208" y2="-3.429" width="0.1524" layer="22"/>
<wire x1="12.065" y1="-3.429" x2="13.208" y2="-3.429" width="0.1524" layer="52"/>
<wire x1="12.065" y1="-3.429" x2="12.065" y2="-2.921" width="0.1524" layer="52"/>
<wire x1="10.795" y1="-12.065" x2="12.065" y2="-12.065" width="0.1524" layer="22"/>
<wire x1="12.065" y1="-12.065" x2="12.065" y2="-10.795" width="0.1524" layer="22"/>
<wire x1="12.065" y1="-1.651" x2="12.065" y2="-2.159" width="0.1524" layer="52"/>
<wire x1="-10.795" y1="12.065" x2="-10.541" y2="12.065" width="0.1524" layer="52"/>
<wire x1="-12.065" y1="10.541" x2="-12.065" y2="10.795" width="0.1524" layer="52"/>
<wire x1="-12.065" y1="2.159" x2="-12.065" y2="1.651" width="0.1524" layer="52"/>
<wire x1="13.208" y1="-6.731" x2="12.065" y2="-6.731" width="0.1524" layer="52"/>
<wire x1="13.208" y1="-6.731" x2="14.224" y2="-6.731" width="0.1524" layer="22"/>
<wire x1="14.224" y1="-6.731" x2="14.224" y2="-5.969" width="0.1524" layer="22"/>
<wire x1="14.224" y1="-5.969" x2="13.208" y2="-5.969" width="0.1524" layer="22"/>
<wire x1="12.065" y1="-5.969" x2="13.208" y2="-5.969" width="0.1524" layer="52"/>
<wire x1="12.065" y1="-5.969" x2="12.065" y2="-5.461" width="0.1524" layer="52"/>
<wire x1="12.065" y1="5.461" x2="12.065" y2="5.969" width="0.1524" layer="52"/>
<wire x1="13.208" y1="5.969" x2="12.065" y2="5.969" width="0.1524" layer="52"/>
<wire x1="13.208" y1="5.969" x2="14.224" y2="5.969" width="0.1524" layer="22"/>
<wire x1="14.224" y1="5.969" x2="14.224" y2="6.731" width="0.1524" layer="22"/>
<wire x1="14.224" y1="6.731" x2="13.208" y2="6.731" width="0.1524" layer="22"/>
<wire x1="12.065" y1="6.731" x2="13.208" y2="6.731" width="0.1524" layer="52"/>
<wire x1="6.731" y1="13.208" x2="6.731" y2="12.065" width="0.1524" layer="52"/>
<wire x1="6.731" y1="13.208" x2="6.731" y2="14.224" width="0.1524" layer="22"/>
<wire x1="6.731" y1="14.224" x2="5.969" y2="14.224" width="0.1524" layer="22"/>
<wire x1="5.969" y1="14.224" x2="5.969" y2="13.208" width="0.1524" layer="22"/>
<wire x1="5.969" y1="12.065" x2="5.969" y2="13.208" width="0.1524" layer="52"/>
<wire x1="5.969" y1="12.065" x2="5.461" y2="12.065" width="0.1524" layer="52"/>
<wire x1="-5.461" y1="12.065" x2="-5.969" y2="12.065" width="0.1524" layer="52"/>
<wire x1="-5.969" y1="13.208" x2="-5.969" y2="12.065" width="0.1524" layer="52"/>
<wire x1="-5.969" y1="13.208" x2="-5.969" y2="14.224" width="0.1524" layer="22"/>
<wire x1="-5.969" y1="14.224" x2="-6.731" y2="14.224" width="0.1524" layer="22"/>
<wire x1="-6.731" y1="14.224" x2="-6.731" y2="13.208" width="0.1524" layer="22"/>
<wire x1="-6.731" y1="12.065" x2="-6.731" y2="13.208" width="0.1524" layer="52"/>
<wire x1="-13.208" y1="6.731" x2="-12.065" y2="6.731" width="0.1524" layer="52"/>
<wire x1="-13.208" y1="6.731" x2="-14.224" y2="6.731" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="6.731" x2="-14.224" y2="5.969" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="5.969" x2="-13.208" y2="5.969" width="0.1524" layer="22"/>
<wire x1="-12.065" y1="5.969" x2="-13.208" y2="5.969" width="0.1524" layer="52"/>
<wire x1="-12.065" y1="5.969" x2="-12.065" y2="5.461" width="0.1524" layer="52"/>
<wire x1="5.461" y1="-12.065" x2="5.969" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="5.969" y1="-13.208" x2="5.969" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="5.969" y1="-13.208" x2="5.969" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="5.969" y1="-14.224" x2="6.731" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="6.731" y1="-14.224" x2="6.731" y2="-13.208" width="0.1524" layer="22"/>
<wire x1="6.731" y1="-12.065" x2="6.731" y2="-13.208" width="0.1524" layer="52"/>
<wire x1="-6.731" y1="-13.208" x2="-6.731" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="-6.731" y1="-13.208" x2="-6.731" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="-6.731" y1="-14.224" x2="-5.969" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="-5.969" y1="-14.224" x2="-5.969" y2="-13.208" width="0.1524" layer="22"/>
<wire x1="-5.969" y1="-12.065" x2="-5.969" y2="-13.208" width="0.1524" layer="52"/>
<wire x1="-5.969" y1="-12.065" x2="-5.461" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="-12.065" y1="-5.461" x2="-12.065" y2="-5.969" width="0.1524" layer="52"/>
<wire x1="-13.208" y1="-5.969" x2="-12.065" y2="-5.969" width="0.1524" layer="52"/>
<wire x1="-13.208" y1="-5.969" x2="-14.224" y2="-5.969" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="-5.969" x2="-14.224" y2="-6.731" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="-6.731" x2="-13.208" y2="-6.731" width="0.1524" layer="22"/>
<wire x1="-12.065" y1="-6.731" x2="-13.208" y2="-6.731" width="0.1524" layer="52"/>
<wire x1="12.065" y1="8.001" x2="12.065" y2="8.509" width="0.1524" layer="52"/>
<wire x1="13.208" y1="8.509" x2="12.065" y2="8.509" width="0.1524" layer="52"/>
<wire x1="13.208" y1="8.509" x2="14.224" y2="8.509" width="0.1524" layer="22"/>
<wire x1="14.224" y1="8.509" x2="14.224" y2="9.271" width="0.1524" layer="22"/>
<wire x1="14.224" y1="9.271" x2="13.208" y2="9.271" width="0.1524" layer="22"/>
<wire x1="12.065" y1="9.271" x2="13.208" y2="9.271" width="0.1524" layer="52"/>
<wire x1="12.065" y1="9.779" x2="12.065" y2="9.271" width="0.1524" layer="52"/>
<wire x1="12.065" y1="6.731" x2="12.065" y2="7.239" width="0.1524" layer="52"/>
<wire x1="6.731" y1="12.065" x2="7.239" y2="12.065" width="0.1524" layer="52"/>
<wire x1="9.271" y1="13.208" x2="9.271" y2="12.065" width="0.1524" layer="52"/>
<wire x1="9.271" y1="13.208" x2="9.271" y2="14.224" width="0.1524" layer="22"/>
<wire x1="9.271" y1="14.224" x2="8.509" y2="14.224" width="0.1524" layer="22"/>
<wire x1="8.509" y1="14.224" x2="8.509" y2="13.208" width="0.1524" layer="22"/>
<wire x1="8.509" y1="12.065" x2="8.509" y2="13.208" width="0.1524" layer="52"/>
<wire x1="8.001" y1="12.065" x2="8.509" y2="12.065" width="0.1524" layer="52"/>
<wire x1="9.779" y1="12.065" x2="9.271" y2="12.065" width="0.1524" layer="52"/>
<wire x1="-8.001" y1="12.065" x2="-8.509" y2="12.065" width="0.1524" layer="52"/>
<wire x1="-8.509" y1="13.208" x2="-8.509" y2="12.065" width="0.1524" layer="52"/>
<wire x1="-8.509" y1="13.208" x2="-8.509" y2="14.224" width="0.1524" layer="22"/>
<wire x1="-8.509" y1="14.224" x2="-9.271" y2="14.224" width="0.1524" layer="22"/>
<wire x1="-9.271" y1="14.224" x2="-9.271" y2="13.208" width="0.1524" layer="22"/>
<wire x1="-9.271" y1="12.065" x2="-9.271" y2="13.208" width="0.1524" layer="52"/>
<wire x1="-9.271" y1="12.065" x2="-9.779" y2="12.065" width="0.1524" layer="52"/>
<wire x1="-6.731" y1="12.065" x2="-7.239" y2="12.065" width="0.1524" layer="52"/>
<wire x1="12.065" y1="-6.731" x2="12.065" y2="-7.239" width="0.1524" layer="52"/>
<wire x1="12.065" y1="-9.779" x2="12.065" y2="-9.271" width="0.1524" layer="52"/>
<wire x1="13.208" y1="-9.271" x2="12.065" y2="-9.271" width="0.1524" layer="52"/>
<wire x1="13.208" y1="-9.271" x2="14.224" y2="-9.271" width="0.1524" layer="22"/>
<wire x1="14.224" y1="-9.271" x2="14.224" y2="-8.509" width="0.1524" layer="22"/>
<wire x1="14.224" y1="-8.509" x2="13.208" y2="-8.509" width="0.1524" layer="22"/>
<wire x1="12.065" y1="-8.509" x2="13.208" y2="-8.509" width="0.1524" layer="52"/>
<wire x1="12.065" y1="-8.001" x2="12.065" y2="-8.509" width="0.1524" layer="52"/>
<wire x1="9.779" y1="-12.065" x2="9.271" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="9.271" y1="-13.208" x2="9.271" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="-9.779" y1="-12.065" x2="-9.271" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="-9.271" y1="-13.208" x2="-9.271" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="-9.271" y1="-13.208" x2="-9.271" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="-9.271" y1="-14.224" x2="-8.509" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="-8.509" y1="-14.224" x2="-8.509" y2="-13.208" width="0.1524" layer="22"/>
<wire x1="-8.509" y1="-12.065" x2="-8.509" y2="-13.208" width="0.1524" layer="52"/>
<wire x1="-8.001" y1="-12.065" x2="-8.509" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="8.001" y1="-12.065" x2="8.509" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="8.509" y1="-13.208" x2="8.509" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="8.509" y1="-13.208" x2="8.509" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="8.509" y1="-14.224" x2="9.271" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="9.271" y1="-14.224" x2="9.271" y2="-13.208" width="0.1524" layer="22"/>
<wire x1="6.731" y1="-12.065" x2="7.239" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="-6.731" y1="-12.065" x2="-7.239" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="-12.065" y1="-8.001" x2="-12.065" y2="-8.509" width="0.1524" layer="52"/>
<wire x1="-13.208" y1="-8.509" x2="-12.065" y2="-8.509" width="0.1524" layer="52"/>
<wire x1="-13.208" y1="-8.509" x2="-14.224" y2="-8.509" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="-8.509" x2="-14.224" y2="-9.271" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="-9.271" x2="-13.208" y2="-9.271" width="0.1524" layer="22"/>
<wire x1="-12.065" y1="-9.271" x2="-13.208" y2="-9.271" width="0.1524" layer="52"/>
<wire x1="-12.065" y1="-9.271" x2="-12.065" y2="-9.779" width="0.1524" layer="52"/>
<wire x1="-12.065" y1="-6.731" x2="-12.065" y2="-7.239" width="0.1524" layer="52"/>
<wire x1="-12.065" y1="6.731" x2="-12.065" y2="7.239" width="0.1524" layer="52"/>
<wire x1="-12.065" y1="9.779" x2="-12.065" y2="9.271" width="0.1524" layer="52"/>
<wire x1="-13.208" y1="9.271" x2="-12.065" y2="9.271" width="0.1524" layer="52"/>
<wire x1="-13.208" y1="9.271" x2="-14.224" y2="9.271" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="9.271" x2="-14.224" y2="8.509" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="8.509" x2="-13.208" y2="8.509" width="0.1524" layer="22"/>
<wire x1="-12.065" y1="8.509" x2="-13.208" y2="8.509" width="0.1524" layer="52"/>
<wire x1="-12.065" y1="8.001" x2="-12.065" y2="8.509" width="0.1524" layer="52"/>
<wire x1="-5.715" y1="-7.62" x2="-7.62" y2="-7.62" width="0.1524" layer="22"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="-5.715" width="0.1524" layer="22"/>
<wire x1="-7.62" y1="-5.715" x2="-5.715" y2="-7.62" width="0.1524" layer="22"/>
<wire x1="5.715" y1="-7.62" x2="7.62" y2="-7.62" width="0.1524" layer="22"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="-5.715" width="0.1524" layer="22"/>
<wire x1="7.62" y1="-5.715" x2="5.715" y2="-7.62" width="0.1524" layer="22"/>
<wire x1="-7.62" y1="5.715" x2="-7.62" y2="7.62" width="0.1524" layer="22"/>
<wire x1="-7.62" y1="7.62" x2="-5.715" y2="7.62" width="0.1524" layer="22"/>
<wire x1="-5.715" y1="7.62" x2="-7.62" y2="5.715" width="0.1524" layer="22"/>
<wire x1="5.715" y1="7.62" x2="7.62" y2="7.62" width="0.1524" layer="22"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="5.715" width="0.1524" layer="22"/>
<wire x1="7.62" y1="5.715" x2="5.715" y2="7.62" width="0.1524" layer="22"/>
<wire x1="-10.541" y1="-13.716" x2="-10.541" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="-10.541" y1="-12.065" x2="-10.795" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="-10.541" y1="-13.716" x2="-10.541" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="-10.541" y1="-14.224" x2="-9.779" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="-9.779" y1="-14.224" x2="-9.779" y2="-13.716" width="0.1524" layer="22"/>
<wire x1="-9.779" y1="-12.065" x2="-9.779" y2="-13.716" width="0.1524" layer="52"/>
<wire x1="-5.461" y1="-13.716" x2="-5.461" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="-5.461" y1="-13.716" x2="-5.461" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="-5.461" y1="-14.224" x2="-4.699" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="-4.699" y1="-14.224" x2="-4.699" y2="-13.716" width="0.1524" layer="22"/>
<wire x1="-4.699" y1="-12.065" x2="-4.699" y2="-13.716" width="0.1524" layer="52"/>
<wire x1="-2.921" y1="-13.716" x2="-2.921" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="-2.921" y1="-13.716" x2="-2.921" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="-2.921" y1="-14.224" x2="-2.159" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="-2.159" y1="-14.224" x2="-2.159" y2="-13.716" width="0.1524" layer="22"/>
<wire x1="-2.159" y1="-12.065" x2="-2.159" y2="-13.716" width="0.1524" layer="52"/>
<wire x1="-0.381" y1="-13.716" x2="-0.381" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="-0.381" y1="-13.716" x2="-0.381" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="-0.381" y1="-14.224" x2="0.381" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="0.381" y1="-14.224" x2="0.381" y2="-13.716" width="0.1524" layer="22"/>
<wire x1="0.381" y1="-12.065" x2="0.381" y2="-13.716" width="0.1524" layer="52"/>
<wire x1="13.716" y1="10.541" x2="12.065" y2="10.541" width="0.1524" layer="52"/>
<wire x1="12.065" y1="10.541" x2="12.065" y2="10.795" width="0.1524" layer="52"/>
<wire x1="13.716" y1="10.541" x2="14.224" y2="10.541" width="0.1524" layer="22"/>
<wire x1="14.224" y1="10.541" x2="14.224" y2="9.779" width="0.1524" layer="22"/>
<wire x1="14.224" y1="9.779" x2="13.716" y2="9.779" width="0.1524" layer="22"/>
<wire x1="12.065" y1="9.779" x2="13.716" y2="9.779" width="0.1524" layer="52"/>
<wire x1="13.716" y1="5.461" x2="12.065" y2="5.461" width="0.1524" layer="52"/>
<wire x1="13.716" y1="5.461" x2="14.224" y2="5.461" width="0.1524" layer="22"/>
<wire x1="14.224" y1="5.461" x2="14.224" y2="4.699" width="0.1524" layer="22"/>
<wire x1="14.224" y1="4.699" x2="13.716" y2="4.699" width="0.1524" layer="22"/>
<wire x1="12.065" y1="4.699" x2="13.716" y2="4.699" width="0.1524" layer="52"/>
<wire x1="13.716" y1="2.921" x2="12.065" y2="2.921" width="0.1524" layer="52"/>
<wire x1="13.716" y1="2.921" x2="14.224" y2="2.921" width="0.1524" layer="22"/>
<wire x1="14.224" y1="2.921" x2="14.224" y2="2.159" width="0.1524" layer="22"/>
<wire x1="14.224" y1="2.159" x2="13.716" y2="2.159" width="0.1524" layer="22"/>
<wire x1="12.065" y1="2.159" x2="13.716" y2="2.159" width="0.1524" layer="52"/>
<wire x1="13.716" y1="0.381" x2="12.065" y2="0.381" width="0.1524" layer="52"/>
<wire x1="13.716" y1="0.381" x2="14.224" y2="0.381" width="0.1524" layer="22"/>
<wire x1="14.224" y1="0.381" x2="14.224" y2="-0.381" width="0.1524" layer="22"/>
<wire x1="14.224" y1="-0.381" x2="13.716" y2="-0.381" width="0.1524" layer="22"/>
<wire x1="12.065" y1="-0.381" x2="13.716" y2="-0.381" width="0.1524" layer="52"/>
<wire x1="-13.716" y1="-10.541" x2="-12.065" y2="-10.541" width="0.1524" layer="52"/>
<wire x1="-12.065" y1="-10.541" x2="-12.065" y2="-10.795" width="0.1524" layer="52"/>
<wire x1="-13.716" y1="-10.541" x2="-14.224" y2="-10.541" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="-10.541" x2="-14.224" y2="-9.779" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="-9.779" x2="-13.716" y2="-9.779" width="0.1524" layer="22"/>
<wire x1="-13.716" y1="-5.461" x2="-12.065" y2="-5.461" width="0.1524" layer="52"/>
<wire x1="-13.716" y1="-5.461" x2="-14.224" y2="-5.461" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="-5.461" x2="-14.224" y2="-4.699" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="-4.699" x2="-13.716" y2="-4.699" width="0.1524" layer="22"/>
<wire x1="-12.065" y1="-4.699" x2="-13.716" y2="-4.699" width="0.1524" layer="52"/>
<wire x1="-13.716" y1="-2.921" x2="-12.065" y2="-2.921" width="0.1524" layer="52"/>
<wire x1="-13.716" y1="-2.921" x2="-14.224" y2="-2.921" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="-2.921" x2="-14.224" y2="-2.159" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="-2.159" x2="-13.716" y2="-2.159" width="0.1524" layer="22"/>
<wire x1="-12.065" y1="-2.159" x2="-13.716" y2="-2.159" width="0.1524" layer="52"/>
<wire x1="-13.716" y1="-0.381" x2="-12.065" y2="-0.381" width="0.1524" layer="52"/>
<wire x1="-13.716" y1="-0.381" x2="-14.224" y2="-0.381" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="-0.381" x2="-14.224" y2="0.381" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="0.381" x2="-13.716" y2="0.381" width="0.1524" layer="22"/>
<wire x1="-12.065" y1="0.381" x2="-13.716" y2="0.381" width="0.1524" layer="52"/>
<wire x1="10.541" y1="13.716" x2="10.541" y2="12.065" width="0.1524" layer="52"/>
<wire x1="10.541" y1="13.716" x2="10.541" y2="14.224" width="0.1524" layer="22"/>
<wire x1="10.541" y1="14.224" x2="9.779" y2="14.224" width="0.1524" layer="22"/>
<wire x1="9.779" y1="14.224" x2="9.779" y2="13.716" width="0.1524" layer="22"/>
<wire x1="9.779" y1="12.065" x2="9.779" y2="13.716" width="0.1524" layer="52"/>
<wire x1="5.461" y1="13.716" x2="5.461" y2="12.065" width="0.1524" layer="52"/>
<wire x1="5.461" y1="13.716" x2="5.461" y2="14.224" width="0.1524" layer="22"/>
<wire x1="5.461" y1="14.224" x2="4.699" y2="14.224" width="0.1524" layer="22"/>
<wire x1="4.699" y1="14.224" x2="4.699" y2="13.716" width="0.1524" layer="22"/>
<wire x1="4.699" y1="12.065" x2="4.699" y2="13.716" width="0.1524" layer="52"/>
<wire x1="2.921" y1="13.716" x2="2.921" y2="12.065" width="0.1524" layer="52"/>
<wire x1="2.921" y1="13.716" x2="2.921" y2="14.224" width="0.1524" layer="22"/>
<wire x1="2.921" y1="14.224" x2="2.159" y2="14.224" width="0.1524" layer="22"/>
<wire x1="2.159" y1="14.224" x2="2.159" y2="13.716" width="0.1524" layer="22"/>
<wire x1="2.159" y1="12.065" x2="2.159" y2="13.716" width="0.1524" layer="52"/>
<wire x1="0.381" y1="13.716" x2="0.381" y2="12.065" width="0.1524" layer="52"/>
<wire x1="0.381" y1="13.716" x2="0.381" y2="14.224" width="0.1524" layer="22"/>
<wire x1="0.381" y1="14.224" x2="-0.381" y2="14.224" width="0.1524" layer="22"/>
<wire x1="-0.381" y1="14.224" x2="-0.381" y2="13.716" width="0.1524" layer="22"/>
<wire x1="-0.381" y1="12.065" x2="-0.381" y2="13.716" width="0.1524" layer="52"/>
<wire x1="-2.159" y1="13.716" x2="-2.159" y2="12.065" width="0.1524" layer="52"/>
<wire x1="-2.159" y1="13.716" x2="-2.159" y2="14.224" width="0.1524" layer="22"/>
<wire x1="-2.159" y1="14.224" x2="-2.921" y2="14.224" width="0.1524" layer="22"/>
<wire x1="-2.921" y1="14.224" x2="-2.921" y2="13.716" width="0.1524" layer="22"/>
<wire x1="-2.921" y1="12.065" x2="-2.921" y2="13.716" width="0.1524" layer="52"/>
<wire x1="-4.699" y1="13.716" x2="-4.699" y2="12.065" width="0.1524" layer="52"/>
<wire x1="-4.699" y1="13.716" x2="-4.699" y2="14.224" width="0.1524" layer="22"/>
<wire x1="-4.699" y1="14.224" x2="-5.461" y2="14.224" width="0.1524" layer="22"/>
<wire x1="-5.461" y1="14.224" x2="-5.461" y2="13.716" width="0.1524" layer="22"/>
<wire x1="-5.461" y1="12.065" x2="-5.461" y2="13.716" width="0.1524" layer="52"/>
<wire x1="-9.779" y1="13.716" x2="-9.779" y2="12.065" width="0.1524" layer="52"/>
<wire x1="-9.779" y1="13.716" x2="-9.779" y2="14.224" width="0.1524" layer="22"/>
<wire x1="-9.779" y1="14.224" x2="-10.541" y2="14.224" width="0.1524" layer="22"/>
<wire x1="-10.541" y1="14.224" x2="-10.541" y2="13.716" width="0.1524" layer="22"/>
<wire x1="-10.541" y1="12.065" x2="-10.541" y2="13.716" width="0.1524" layer="52"/>
<wire x1="2.159" y1="-13.716" x2="2.159" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="2.159" y1="-13.716" x2="2.159" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="2.159" y1="-14.224" x2="2.921" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="2.921" y1="-14.224" x2="2.921" y2="-13.716" width="0.1524" layer="22"/>
<wire x1="2.921" y1="-12.065" x2="2.921" y2="-13.716" width="0.1524" layer="52"/>
<wire x1="4.699" y1="-13.716" x2="4.699" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="4.699" y1="-13.716" x2="4.699" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="4.699" y1="-14.224" x2="5.461" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="5.461" y1="-14.224" x2="5.461" y2="-13.716" width="0.1524" layer="22"/>
<wire x1="5.461" y1="-12.065" x2="5.461" y2="-13.716" width="0.1524" layer="52"/>
<wire x1="9.779" y1="-13.716" x2="9.779" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="9.779" y1="-13.716" x2="9.779" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="9.779" y1="-14.224" x2="10.541" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="10.541" y1="-14.224" x2="10.541" y2="-13.716" width="0.1524" layer="22"/>
<wire x1="10.795" y1="-12.065" x2="10.541" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="10.541" y1="-12.065" x2="10.541" y2="-13.716" width="0.1524" layer="52"/>
<wire x1="-13.716" y1="10.541" x2="-12.065" y2="10.541" width="0.1524" layer="52"/>
<wire x1="-13.716" y1="10.541" x2="-14.224" y2="10.541" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="10.541" x2="-14.224" y2="9.779" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="9.779" x2="-13.716" y2="9.779" width="0.1524" layer="22"/>
<wire x1="-13.716" y1="5.461" x2="-12.065" y2="5.461" width="0.1524" layer="52"/>
<wire x1="-13.716" y1="5.461" x2="-14.224" y2="5.461" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="5.461" x2="-14.224" y2="4.699" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="4.699" x2="-13.716" y2="4.699" width="0.1524" layer="22"/>
<wire x1="-12.065" y1="4.699" x2="-13.716" y2="4.699" width="0.1524" layer="52"/>
<wire x1="-13.716" y1="2.921" x2="-12.065" y2="2.921" width="0.1524" layer="52"/>
<wire x1="-13.716" y1="2.921" x2="-14.224" y2="2.921" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="2.921" x2="-14.224" y2="2.159" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="2.159" x2="-13.716" y2="2.159" width="0.1524" layer="22"/>
<wire x1="-12.065" y1="2.159" x2="-13.716" y2="2.159" width="0.1524" layer="52"/>
<wire x1="13.716" y1="-10.541" x2="12.065" y2="-10.541" width="0.1524" layer="52"/>
<wire x1="12.065" y1="-10.541" x2="12.065" y2="-10.795" width="0.1524" layer="52"/>
<wire x1="13.716" y1="-10.541" x2="14.224" y2="-10.541" width="0.1524" layer="22"/>
<wire x1="14.224" y1="-10.541" x2="14.224" y2="-9.779" width="0.1524" layer="22"/>
<wire x1="14.224" y1="-9.779" x2="13.716" y2="-9.779" width="0.1524" layer="22"/>
<wire x1="13.716" y1="-5.461" x2="12.065" y2="-5.461" width="0.1524" layer="52"/>
<wire x1="13.716" y1="-5.461" x2="14.224" y2="-5.461" width="0.1524" layer="22"/>
<wire x1="14.224" y1="-5.461" x2="14.224" y2="-4.699" width="0.1524" layer="22"/>
<wire x1="14.224" y1="-4.699" x2="13.716" y2="-4.699" width="0.1524" layer="22"/>
<wire x1="12.065" y1="-4.699" x2="13.716" y2="-4.699" width="0.1524" layer="52"/>
<wire x1="13.716" y1="-2.921" x2="12.065" y2="-2.921" width="0.1524" layer="52"/>
<wire x1="13.716" y1="-2.921" x2="14.224" y2="-2.921" width="0.1524" layer="22"/>
<wire x1="14.224" y1="-2.921" x2="14.224" y2="-2.159" width="0.1524" layer="22"/>
<wire x1="14.224" y1="-2.159" x2="13.716" y2="-2.159" width="0.1524" layer="22"/>
<wire x1="12.065" y1="-2.159" x2="13.716" y2="-2.159" width="0.1524" layer="52"/>
<wire x1="13.716" y1="7.239" x2="12.065" y2="7.239" width="0.1524" layer="52"/>
<wire x1="13.716" y1="7.239" x2="14.224" y2="7.239" width="0.1524" layer="22"/>
<wire x1="14.224" y1="7.239" x2="14.224" y2="8.001" width="0.1524" layer="22"/>
<wire x1="14.224" y1="8.001" x2="13.716" y2="8.001" width="0.1524" layer="22"/>
<wire x1="12.065" y1="8.001" x2="13.716" y2="8.001" width="0.1524" layer="52"/>
<wire x1="7.239" y1="13.716" x2="7.239" y2="12.065" width="0.1524" layer="52"/>
<wire x1="7.239" y1="13.716" x2="7.239" y2="14.224" width="0.1524" layer="22"/>
<wire x1="7.239" y1="14.224" x2="8.001" y2="14.224" width="0.1524" layer="22"/>
<wire x1="8.001" y1="14.224" x2="8.001" y2="13.716" width="0.1524" layer="22"/>
<wire x1="8.001" y1="12.065" x2="8.001" y2="13.716" width="0.1524" layer="52"/>
<wire x1="-7.239" y1="13.716" x2="-7.239" y2="12.065" width="0.1524" layer="52"/>
<wire x1="-7.239" y1="13.716" x2="-7.239" y2="14.224" width="0.1524" layer="22"/>
<wire x1="-7.239" y1="14.224" x2="-8.001" y2="14.224" width="0.1524" layer="22"/>
<wire x1="-8.001" y1="14.224" x2="-8.001" y2="13.716" width="0.1524" layer="22"/>
<wire x1="-8.001" y1="12.065" x2="-8.001" y2="13.716" width="0.1524" layer="52"/>
<wire x1="13.716" y1="-7.239" x2="12.065" y2="-7.239" width="0.1524" layer="52"/>
<wire x1="13.716" y1="-7.239" x2="14.224" y2="-7.239" width="0.1524" layer="22"/>
<wire x1="14.224" y1="-7.239" x2="14.224" y2="-8.001" width="0.1524" layer="22"/>
<wire x1="14.224" y1="-8.001" x2="13.716" y2="-8.001" width="0.1524" layer="22"/>
<wire x1="12.065" y1="-8.001" x2="13.716" y2="-8.001" width="0.1524" layer="52"/>
<wire x1="12.065" y1="-9.779" x2="13.716" y2="-9.779" width="0.1524" layer="52"/>
<wire x1="7.239" y1="-13.716" x2="7.239" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="7.239" y1="-13.716" x2="7.239" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="7.239" y1="-14.224" x2="8.001" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="8.001" y1="-14.224" x2="8.001" y2="-13.716" width="0.1524" layer="22"/>
<wire x1="8.001" y1="-12.065" x2="8.001" y2="-13.716" width="0.1524" layer="52"/>
<wire x1="-7.239" y1="-13.716" x2="-7.239" y2="-12.065" width="0.1524" layer="52"/>
<wire x1="-7.239" y1="-13.716" x2="-7.239" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="-7.239" y1="-14.224" x2="-8.001" y2="-14.224" width="0.1524" layer="22"/>
<wire x1="-8.001" y1="-14.224" x2="-8.001" y2="-13.716" width="0.1524" layer="22"/>
<wire x1="-8.001" y1="-12.065" x2="-8.001" y2="-13.716" width="0.1524" layer="52"/>
<wire x1="-13.716" y1="-7.239" x2="-12.065" y2="-7.239" width="0.1524" layer="52"/>
<wire x1="-13.716" y1="-7.239" x2="-14.224" y2="-7.239" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="-7.239" x2="-14.224" y2="-8.001" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="-8.001" x2="-13.716" y2="-8.001" width="0.1524" layer="22"/>
<wire x1="-12.065" y1="-8.001" x2="-13.716" y2="-8.001" width="0.1524" layer="52"/>
<wire x1="-13.716" y1="-9.779" x2="-12.065" y2="-9.779" width="0.1524" layer="52"/>
<wire x1="-13.716" y1="7.239" x2="-12.065" y2="7.239" width="0.1524" layer="52"/>
<wire x1="-13.716" y1="7.239" x2="-14.224" y2="7.239" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="7.239" x2="-14.224" y2="8.001" width="0.1524" layer="22"/>
<wire x1="-14.224" y1="8.001" x2="-13.716" y2="8.001" width="0.1524" layer="22"/>
<wire x1="-12.065" y1="8.001" x2="-13.716" y2="8.001" width="0.1524" layer="52"/>
<wire x1="-12.065" y1="9.779" x2="-13.716" y2="9.779" width="0.1524" layer="52"/>
<wire x1="-15.494" y1="-14.859" x2="-14.859" y2="-15.494" width="0.1524" layer="22" curve="90"/>
<wire x1="14.859" y1="-15.494" x2="-14.859" y2="-15.494" width="0.1524" layer="22"/>
<wire x1="14.859" y1="-15.494" x2="15.494" y2="-14.859" width="0.1524" layer="22" curve="90"/>
<wire x1="14.859" y1="15.494" x2="-13.97" y2="15.494" width="0.1524" layer="22"/>
<wire x1="15.494" y1="14.859" x2="15.494" y2="-14.859" width="0.1524" layer="22"/>
<wire x1="14.859" y1="15.494" x2="15.494" y2="14.859" width="0.1524" layer="22" curve="-90"/>
<wire x1="-6.35" y1="-1.27" x2="-6.35" y2="1.27" width="0.1524" layer="22"/>
<wire x1="-6.35" y1="1.27" x2="-7.62" y2="0" width="0.1524" layer="22"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="-1.27" width="0.1524" layer="22"/>
<circle x="3.81" y="3.81" radius="2.54" width="0.1524" layer="22"/>
<circle x="-3.81" y="3.81" radius="2.54" width="0.1524" layer="22"/>
<circle x="-3.81" y="-3.81" radius="2.54" width="0.1524" layer="22"/>
<circle x="3.81" y="-3.81" radius="2.54" width="0.1524" layer="22"/>
<pad name="1" x="0" y="12.7" drill="0.8128"/>
<pad name="68" x="0" y="10.16" drill="0.8128" shape="octagon"/>
<pad name="3" x="-2.54" y="12.7" drill="0.8128" shape="octagon"/>
<pad name="2" x="-2.54" y="10.16" drill="0.8128" shape="octagon"/>
<pad name="5" x="-5.08" y="12.7" drill="0.8128" shape="octagon"/>
<pad name="4" x="-5.08" y="10.16" drill="0.8128" shape="octagon"/>
<pad name="7" x="-7.62" y="12.7" drill="0.8128" shape="octagon"/>
<pad name="6" x="-7.62" y="10.16" drill="0.8128" shape="octagon"/>
<pad name="9" x="-10.16" y="12.7" drill="0.8128" shape="octagon"/>
<pad name="10" x="-12.7" y="10.16" drill="0.8128" shape="octagon"/>
<pad name="8" x="-10.16" y="10.16" drill="0.8128" shape="octagon"/>
<pad name="12" x="-12.7" y="7.62" drill="0.8128" shape="octagon"/>
<pad name="11" x="-10.16" y="7.62" drill="0.8128" shape="octagon"/>
<pad name="14" x="-12.7" y="5.08" drill="0.8128" shape="octagon"/>
<pad name="13" x="-10.16" y="5.08" drill="0.8128" shape="octagon"/>
<pad name="16" x="-12.7" y="2.54" drill="0.8128" shape="octagon"/>
<pad name="15" x="-10.16" y="2.54" drill="0.8128" shape="octagon"/>
<pad name="18" x="-12.7" y="0" drill="0.8128" shape="octagon"/>
<pad name="17" x="-10.16" y="0" drill="0.8128" shape="octagon"/>
<pad name="20" x="-12.7" y="-2.54" drill="0.8128" shape="octagon"/>
<pad name="19" x="-10.16" y="-2.54" drill="0.8128" shape="octagon"/>
<pad name="22" x="-12.7" y="-5.08" drill="0.8128" shape="octagon"/>
<pad name="21" x="-10.16" y="-5.08" drill="0.8128" shape="octagon"/>
<pad name="24" x="-12.7" y="-7.62" drill="0.8128" shape="octagon"/>
<pad name="23" x="-10.16" y="-7.62" drill="0.8128" shape="octagon"/>
<pad name="26" x="-12.7" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="27" x="-10.16" y="-12.7" drill="0.8128" shape="octagon"/>
<pad name="25" x="-10.16" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="29" x="-7.62" y="-12.7" drill="0.8128" shape="octagon"/>
<pad name="28" x="-7.62" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="31" x="-5.08" y="-12.7" drill="0.8128" shape="octagon"/>
<pad name="30" x="-5.08" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="33" x="-2.54" y="-12.7" drill="0.8128" shape="octagon"/>
<pad name="32" x="-2.54" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="35" x="0" y="-12.7" drill="0.8128" shape="octagon"/>
<pad name="34" x="0" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="37" x="2.54" y="-12.7" drill="0.8128" shape="octagon"/>
<pad name="36" x="2.54" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="39" x="5.08" y="-12.7" drill="0.8128" shape="octagon"/>
<pad name="38" x="5.08" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="41" x="7.62" y="-12.7" drill="0.8128" shape="octagon"/>
<pad name="40" x="7.62" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="43" x="10.16" y="-12.7" drill="0.8128" shape="octagon"/>
<pad name="44" x="12.7" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="42" x="10.16" y="-10.16" drill="0.8128" shape="octagon"/>
<pad name="46" x="12.7" y="-7.62" drill="0.8128" shape="octagon"/>
<pad name="45" x="10.16" y="-7.62" drill="0.8128" shape="octagon"/>
<pad name="48" x="12.7" y="-5.08" drill="0.8128" shape="octagon"/>
<pad name="47" x="10.16" y="-5.08" drill="0.8128" shape="octagon"/>
<pad name="50" x="12.7" y="-2.54" drill="0.8128" shape="octagon"/>
<pad name="49" x="10.16" y="-2.54" drill="0.8128" shape="octagon"/>
<pad name="52" x="12.7" y="0" drill="0.8128" shape="octagon"/>
<pad name="51" x="10.16" y="0" drill="0.8128" shape="octagon"/>
<pad name="54" x="12.7" y="2.54" drill="0.8128" shape="octagon"/>
<pad name="53" x="10.16" y="2.54" drill="0.8128" shape="octagon"/>
<pad name="56" x="12.7" y="5.08" drill="0.8128" shape="octagon"/>
<pad name="55" x="10.16" y="5.08" drill="0.8128" shape="octagon"/>
<pad name="58" x="12.7" y="7.62" drill="0.8128" shape="octagon"/>
<pad name="57" x="10.16" y="7.62" drill="0.8128" shape="octagon"/>
<pad name="60" x="12.7" y="10.16" drill="0.8128" shape="octagon"/>
<pad name="61" x="10.16" y="12.7" drill="0.8128" shape="octagon"/>
<pad name="59" x="10.16" y="10.16" drill="0.8128" shape="octagon"/>
<pad name="63" x="7.62" y="12.7" drill="0.8128" shape="octagon"/>
<pad name="62" x="7.62" y="10.16" drill="0.8128" shape="octagon"/>
<pad name="65" x="5.08" y="12.7" drill="0.8128" shape="octagon"/>
<pad name="64" x="5.08" y="10.16" drill="0.8128" shape="octagon"/>
<pad name="67" x="2.54" y="12.7" drill="0.8128" shape="octagon"/>
<pad name="66" x="2.54" y="10.16" drill="0.8128" shape="octagon"/>
<text x="-15.24" y="15.875" size="1.27" layer="28" ratio="10" rot="MR0" align="bottom-right">&gt;VALUE</text>
<text x="15.367" y="15.875" size="1.27" layer="26" ratio="10" rot="MR0">&gt;NAME</text>
<text x="14.605" y="-14.605" size="1.27" layer="22" ratio="10" rot="MR0">68</text>
<rectangle x1="-6.1595" y1="-0.9525" x2="-5.1435" y2="0.9525" layer="22" rot="R90"/>
<rectangle x1="-7.4295" y1="-0.1905" x2="-5.6515" y2="0.1905" layer="22" rot="R90"/>
<rectangle x1="-7.493" y1="-0.127" x2="-6.223" y2="0.127" layer="22" rot="R90"/>
<rectangle x1="-7.493" y1="-0.127" x2="-6.731" y2="0.127" layer="22" rot="R90"/>
<rectangle x1="-7.493" y1="-0.127" x2="-7.239" y2="0.127" layer="22" rot="R90"/>
<rectangle x1="-6.5405" y1="-1.0795" x2="-6.2865" y2="-0.9525" layer="22" rot="R90"/>
<rectangle x1="-6.604" y1="-1.016" x2="-6.477" y2="-0.889" layer="22" rot="R90"/>
<rectangle x1="-6.858" y1="-0.762" x2="-6.731" y2="-0.635" layer="22" rot="R90"/>
<rectangle x1="-7.112" y1="-0.508" x2="-6.985" y2="-0.381" layer="22" rot="R90"/>
<rectangle x1="-7.366" y1="-0.254" x2="-7.239" y2="-0.127" layer="22" rot="R90"/>
<rectangle x1="-7.366" y1="0.127" x2="-7.239" y2="0.254" layer="22" rot="R90"/>
<rectangle x1="-7.112" y1="0.381" x2="-6.985" y2="0.508" layer="22" rot="R90"/>
<rectangle x1="-6.858" y1="0.635" x2="-6.731" y2="0.762" layer="22" rot="R90"/>
<rectangle x1="-6.5405" y1="0.9525" x2="-6.2865" y2="1.0795" layer="22" rot="R90"/>
<rectangle x1="-6.604" y1="0.889" x2="-6.477" y2="1.016" layer="22" rot="R90"/>
<text x="0.127" y="-0.635" size="1.27" layer="25" ratio="10" align="bottom-center">&gt;NAME</text>
</package>
<package name="DIL64">
<description>&lt;b&gt;Dual In Line&lt;/b&gt;</description>
<wire x1="-40.64" y1="-1.27" x2="-40.64" y2="-10.414" width="0.1524" layer="21"/>
<wire x1="-40.64" y1="1.27" x2="-40.64" y2="-1.27" width="0.1524" layer="21" curve="-180"/>
<wire x1="40.64" y1="-10.414" x2="40.64" y2="10.414" width="0.1524" layer="21"/>
<wire x1="-40.64" y1="-10.414" x2="40.64" y2="-10.414" width="0.1524" layer="21"/>
<wire x1="-40.64" y1="10.414" x2="-40.64" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-40.64" y1="10.414" x2="40.64" y2="10.414" width="0.1524" layer="21"/>
<pad name="1" x="-39.37" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-36.83" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-34.29" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-31.75" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="-29.21" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="-26.67" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="-24.13" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="-21.59" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="-19.05" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="-16.51" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="-13.97" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="-11.43" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="-8.89" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="-6.35" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="-3.81" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="-1.27" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="17" x="1.27" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="18" x="3.81" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="19" x="6.35" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="32" x="39.37" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="33" x="39.37" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="46" x="6.35" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="47" x="3.81" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="48" x="1.27" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="49" x="-1.27" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="50" x="-3.81" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="51" x="-6.35" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="52" x="-8.89" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="53" x="-11.43" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="54" x="-13.97" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="55" x="-16.51" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="56" x="-19.05" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="57" x="-21.59" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="58" x="-24.13" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="59" x="-26.67" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="60" x="-29.21" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="61" x="-31.75" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="62" x="-34.29" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="63" x="-36.83" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="64" x="-39.37" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="20" x="8.89" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="21" x="11.43" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="22" x="13.97" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="23" x="16.51" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="24" x="19.05" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="25" x="21.59" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="26" x="24.13" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="27" x="26.67" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="28" x="29.21" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="29" x="31.75" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="30" x="34.29" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="31" x="36.83" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="45" x="8.89" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="44" x="11.43" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="43" x="13.97" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="42" x="16.51" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="41" x="19.05" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="40" x="21.59" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="39" x="24.13" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="38" x="26.67" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="37" x="29.21" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="36" x="31.75" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="35" x="34.29" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="34" x="36.83" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<text x="-41.275" y="-10.16" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-36.83" y="-2.2352" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="68000">
<wire x1="-12.7" y1="-50.8" x2="10.16" y2="-50.8" width="0.4064" layer="94"/>
<wire x1="10.16" y1="48.26" x2="10.16" y2="-50.8" width="0.4064" layer="94"/>
<wire x1="10.16" y1="48.26" x2="-12.7" y2="48.26" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-50.8" x2="-12.7" y2="48.26" width="0.4064" layer="94"/>
<text x="-12.7" y="-53.34" size="1.778" layer="96">&gt;VALUE</text>
<text x="-12.7" y="48.895" size="1.778" layer="95">&gt;NAME</text>
<pin name="CLK" x="-17.78" y="45.72" length="middle" direction="in" function="clk"/>
<pin name="VPA" x="-17.78" y="30.48" length="middle" direction="in" function="dot"/>
<pin name="BERR" x="-17.78" y="40.64" length="middle" direction="in" function="dot"/>
<pin name="RESET" x="-17.78" y="38.1" length="middle" function="dot"/>
<pin name="HALT" x="-17.78" y="35.56" length="middle" function="dot"/>
<pin name="DTACK" x="-17.78" y="17.78" length="middle" direction="in" function="dot"/>
<pin name="BR" x="-17.78" y="25.4" length="middle" direction="in" function="dot"/>
<pin name="BGACK" x="-17.78" y="22.86" length="middle" direction="in" function="dot"/>
<pin name="IPL0" x="-17.78" y="12.7" length="middle" direction="in" function="dot"/>
<pin name="D0" x="-17.78" y="-10.16" length="middle"/>
<pin name="D1" x="-17.78" y="-12.7" length="middle"/>
<pin name="D2" x="-17.78" y="-15.24" length="middle"/>
<pin name="D3" x="-17.78" y="-17.78" length="middle"/>
<pin name="D4" x="-17.78" y="-20.32" length="middle"/>
<pin name="D5" x="-17.78" y="-22.86" length="middle"/>
<pin name="D6" x="-17.78" y="-25.4" length="middle"/>
<pin name="D7" x="-17.78" y="-27.94" length="middle"/>
<pin name="D8" x="-17.78" y="-30.48" length="middle"/>
<pin name="D9" x="-17.78" y="-33.02" length="middle"/>
<pin name="D10" x="-17.78" y="-35.56" length="middle"/>
<pin name="D11" x="-17.78" y="-38.1" length="middle"/>
<pin name="D12" x="-17.78" y="-40.64" length="middle"/>
<pin name="D13" x="-17.78" y="-43.18" length="middle"/>
<pin name="D14" x="-17.78" y="-45.72" length="middle"/>
<pin name="D15" x="-17.78" y="-48.26" length="middle"/>
<pin name="E" x="15.24" y="33.02" length="middle" direction="out" rot="R180"/>
<pin name="VMA" x="15.24" y="30.48" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="FC0" x="15.24" y="45.72" length="middle" direction="out" rot="R180"/>
<pin name="FC1" x="15.24" y="43.18" length="middle" direction="out" rot="R180"/>
<pin name="FC2" x="15.24" y="40.64" length="middle" direction="out" rot="R180"/>
<pin name="AS" x="15.24" y="20.32" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="R/W" x="15.24" y="17.78" length="middle" direction="out" rot="R180"/>
<pin name="UDS" x="15.24" y="15.24" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="LDS" x="15.24" y="12.7" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="BG" x="15.24" y="25.4" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="A1" x="15.24" y="7.62" length="middle" direction="out" rot="R180"/>
<pin name="A2" x="15.24" y="5.08" length="middle" direction="out" rot="R180"/>
<pin name="A3" x="15.24" y="2.54" length="middle" direction="out" rot="R180"/>
<pin name="A4" x="15.24" y="0" length="middle" direction="out" rot="R180"/>
<pin name="A5" x="15.24" y="-2.54" length="middle" direction="out" rot="R180"/>
<pin name="A6" x="15.24" y="-5.08" length="middle" direction="out" rot="R180"/>
<pin name="A7" x="15.24" y="-7.62" length="middle" direction="out" rot="R180"/>
<pin name="A9" x="15.24" y="-12.7" length="middle" direction="out" rot="R180"/>
<pin name="A8" x="15.24" y="-10.16" length="middle" direction="out" rot="R180"/>
<pin name="A10" x="15.24" y="-15.24" length="middle" direction="out" rot="R180"/>
<pin name="A11" x="15.24" y="-17.78" length="middle" direction="out" rot="R180"/>
<pin name="A12" x="15.24" y="-20.32" length="middle" direction="out" rot="R180"/>
<pin name="A13" x="15.24" y="-22.86" length="middle" direction="out" rot="R180"/>
<pin name="A14" x="15.24" y="-25.4" length="middle" direction="out" rot="R180"/>
<pin name="A15" x="15.24" y="-27.94" length="middle" direction="out" rot="R180"/>
<pin name="A16" x="15.24" y="-30.48" length="middle" direction="out" rot="R180"/>
<pin name="A17" x="15.24" y="-33.02" length="middle" direction="out" rot="R180"/>
<pin name="A18" x="15.24" y="-35.56" length="middle" direction="out" rot="R180"/>
<pin name="A19" x="15.24" y="-38.1" length="middle" direction="out" rot="R180"/>
<pin name="A20" x="15.24" y="-40.64" length="middle" direction="out" rot="R180"/>
<pin name="A21" x="15.24" y="-43.18" length="middle" direction="out" rot="R180"/>
<pin name="A22" x="15.24" y="-45.72" length="middle" direction="out" rot="R180"/>
<pin name="A23" x="15.24" y="-48.26" length="middle" direction="out" rot="R180"/>
<pin name="IPL1" x="-17.78" y="10.16" length="middle" direction="in" function="dot"/>
<pin name="IPL2" x="-17.78" y="7.62" length="middle" direction="in" function="dot"/>
</symbol>
<symbol name="2PWR4GND">
<text x="9.525" y="-5.842" size="1.27" layer="95" rot="R90">GND</text>
<text x="4.445" y="2.54" size="1.27" layer="95" rot="R90">VCC</text>
<text x="-0.635" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<pin name="VCC@1" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<pin name="VCC@2" x="2.54" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<pin name="GND@4" x="7.62" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@3" x="5.08" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@2" x="2.54" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@1" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
</symbol>
<symbol name="2PWR2GND">
<text x="-0.635" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="4.445" y="2.54" size="1.27" layer="95" rot="R90">VCC</text>
<text x="4.445" y="-5.842" size="1.27" layer="95" rot="R90">GND</text>
<pin name="VCC@1" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<pin name="VCC@2" x="2.54" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<pin name="GND@1" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@2" x="2.54" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MC68000FN" prefix="IC" uservalue="yes">
<description>&lt;b&gt;68xxx PROCESSOR&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="68000" x="0" y="0"/>
<gate name="P" symbol="2PWR4GND" x="-40.64" y="15.24" addlevel="request"/>
</gates>
<devices>
<device name="" package="PLCC68">
<connects>
<connect gate="G$1" pin="A1" pad="32"/>
<connect gate="G$1" pin="A10" pad="41"/>
<connect gate="G$1" pin="A11" pad="42"/>
<connect gate="G$1" pin="A12" pad="43"/>
<connect gate="G$1" pin="A13" pad="44"/>
<connect gate="G$1" pin="A14" pad="45"/>
<connect gate="G$1" pin="A15" pad="46"/>
<connect gate="G$1" pin="A16" pad="47"/>
<connect gate="G$1" pin="A17" pad="48"/>
<connect gate="G$1" pin="A18" pad="49"/>
<connect gate="G$1" pin="A19" pad="50"/>
<connect gate="G$1" pin="A2" pad="33"/>
<connect gate="G$1" pin="A20" pad="51"/>
<connect gate="G$1" pin="A21" pad="53"/>
<connect gate="G$1" pin="A22" pad="54"/>
<connect gate="G$1" pin="A23" pad="55"/>
<connect gate="G$1" pin="A3" pad="34"/>
<connect gate="G$1" pin="A4" pad="35"/>
<connect gate="G$1" pin="A5" pad="36"/>
<connect gate="G$1" pin="A6" pad="37"/>
<connect gate="G$1" pin="A7" pad="38"/>
<connect gate="G$1" pin="A8" pad="39"/>
<connect gate="G$1" pin="A9" pad="40"/>
<connect gate="G$1" pin="AS" pad="6"/>
<connect gate="G$1" pin="BERR" pad="24"/>
<connect gate="G$1" pin="BG" pad="11"/>
<connect gate="G$1" pin="BGACK" pad="12"/>
<connect gate="G$1" pin="BR" pad="13"/>
<connect gate="G$1" pin="CLK" pad="15"/>
<connect gate="G$1" pin="D0" pad="5"/>
<connect gate="G$1" pin="D1" pad="4"/>
<connect gate="G$1" pin="D10" pad="63"/>
<connect gate="G$1" pin="D11" pad="62"/>
<connect gate="G$1" pin="D12" pad="61"/>
<connect gate="G$1" pin="D13" pad="60"/>
<connect gate="G$1" pin="D14" pad="59"/>
<connect gate="G$1" pin="D15" pad="58"/>
<connect gate="G$1" pin="D2" pad="3"/>
<connect gate="G$1" pin="D3" pad="2"/>
<connect gate="G$1" pin="D4" pad="1"/>
<connect gate="G$1" pin="D5" pad="68"/>
<connect gate="G$1" pin="D6" pad="67"/>
<connect gate="G$1" pin="D7" pad="66"/>
<connect gate="G$1" pin="D8" pad="65"/>
<connect gate="G$1" pin="D9" pad="64"/>
<connect gate="G$1" pin="DTACK" pad="10"/>
<connect gate="G$1" pin="E" pad="22"/>
<connect gate="G$1" pin="FC0" pad="30"/>
<connect gate="G$1" pin="FC1" pad="29"/>
<connect gate="G$1" pin="FC2" pad="28"/>
<connect gate="G$1" pin="HALT" pad="19"/>
<connect gate="G$1" pin="IPL0" pad="27"/>
<connect gate="G$1" pin="IPL1" pad="26"/>
<connect gate="G$1" pin="IPL2" pad="25"/>
<connect gate="G$1" pin="LDS" pad="8"/>
<connect gate="G$1" pin="R/W" pad="9"/>
<connect gate="G$1" pin="RESET" pad="20"/>
<connect gate="G$1" pin="UDS" pad="7"/>
<connect gate="G$1" pin="VMA" pad="21"/>
<connect gate="G$1" pin="VPA" pad="23"/>
<connect gate="P" pin="GND@1" pad="16"/>
<connect gate="P" pin="GND@2" pad="17"/>
<connect gate="P" pin="GND@3" pad="56"/>
<connect gate="P" pin="GND@4" pad="57"/>
<connect gate="P" pin="VCC@1" pad="14"/>
<connect gate="P" pin="VCC@2" pad="52"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SOC" package="PLCC68-S">
<connects>
<connect gate="G$1" pin="A1" pad="32"/>
<connect gate="G$1" pin="A10" pad="41"/>
<connect gate="G$1" pin="A11" pad="42"/>
<connect gate="G$1" pin="A12" pad="43"/>
<connect gate="G$1" pin="A13" pad="44"/>
<connect gate="G$1" pin="A14" pad="45"/>
<connect gate="G$1" pin="A15" pad="46"/>
<connect gate="G$1" pin="A16" pad="47"/>
<connect gate="G$1" pin="A17" pad="48"/>
<connect gate="G$1" pin="A18" pad="49"/>
<connect gate="G$1" pin="A19" pad="50"/>
<connect gate="G$1" pin="A2" pad="33"/>
<connect gate="G$1" pin="A20" pad="51"/>
<connect gate="G$1" pin="A21" pad="53"/>
<connect gate="G$1" pin="A22" pad="54"/>
<connect gate="G$1" pin="A23" pad="55"/>
<connect gate="G$1" pin="A3" pad="34"/>
<connect gate="G$1" pin="A4" pad="35"/>
<connect gate="G$1" pin="A5" pad="36"/>
<connect gate="G$1" pin="A6" pad="37"/>
<connect gate="G$1" pin="A7" pad="38"/>
<connect gate="G$1" pin="A8" pad="39"/>
<connect gate="G$1" pin="A9" pad="40"/>
<connect gate="G$1" pin="AS" pad="6"/>
<connect gate="G$1" pin="BERR" pad="24"/>
<connect gate="G$1" pin="BG" pad="11"/>
<connect gate="G$1" pin="BGACK" pad="12"/>
<connect gate="G$1" pin="BR" pad="13"/>
<connect gate="G$1" pin="CLK" pad="15"/>
<connect gate="G$1" pin="D0" pad="5"/>
<connect gate="G$1" pin="D1" pad="4"/>
<connect gate="G$1" pin="D10" pad="63"/>
<connect gate="G$1" pin="D11" pad="62"/>
<connect gate="G$1" pin="D12" pad="61"/>
<connect gate="G$1" pin="D13" pad="60"/>
<connect gate="G$1" pin="D14" pad="59"/>
<connect gate="G$1" pin="D15" pad="58"/>
<connect gate="G$1" pin="D2" pad="3"/>
<connect gate="G$1" pin="D3" pad="2"/>
<connect gate="G$1" pin="D4" pad="1"/>
<connect gate="G$1" pin="D5" pad="68"/>
<connect gate="G$1" pin="D6" pad="67"/>
<connect gate="G$1" pin="D7" pad="66"/>
<connect gate="G$1" pin="D8" pad="65"/>
<connect gate="G$1" pin="D9" pad="64"/>
<connect gate="G$1" pin="DTACK" pad="10"/>
<connect gate="G$1" pin="E" pad="22"/>
<connect gate="G$1" pin="FC0" pad="30"/>
<connect gate="G$1" pin="FC1" pad="29"/>
<connect gate="G$1" pin="FC2" pad="28"/>
<connect gate="G$1" pin="HALT" pad="19"/>
<connect gate="G$1" pin="IPL0" pad="27"/>
<connect gate="G$1" pin="IPL1" pad="26"/>
<connect gate="G$1" pin="IPL2" pad="25"/>
<connect gate="G$1" pin="LDS" pad="8"/>
<connect gate="G$1" pin="R/W" pad="9"/>
<connect gate="G$1" pin="RESET" pad="20"/>
<connect gate="G$1" pin="UDS" pad="7"/>
<connect gate="G$1" pin="VMA" pad="21"/>
<connect gate="G$1" pin="VPA" pad="23"/>
<connect gate="P" pin="GND@1" pad="16"/>
<connect gate="P" pin="GND@2" pad="17"/>
<connect gate="P" pin="GND@3" pad="56"/>
<connect gate="P" pin="GND@4" pad="57"/>
<connect gate="P" pin="VCC@1" pad="14"/>
<connect gate="P" pin="VCC@2" pad="52"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TOPMOUNT" package="PLCC68-S_TOPMOUNT">
<connects>
<connect gate="G$1" pin="A1" pad="32"/>
<connect gate="G$1" pin="A10" pad="41"/>
<connect gate="G$1" pin="A11" pad="42"/>
<connect gate="G$1" pin="A12" pad="43"/>
<connect gate="G$1" pin="A13" pad="44"/>
<connect gate="G$1" pin="A14" pad="45"/>
<connect gate="G$1" pin="A15" pad="46"/>
<connect gate="G$1" pin="A16" pad="47"/>
<connect gate="G$1" pin="A17" pad="48"/>
<connect gate="G$1" pin="A18" pad="49"/>
<connect gate="G$1" pin="A19" pad="50"/>
<connect gate="G$1" pin="A2" pad="33"/>
<connect gate="G$1" pin="A20" pad="51"/>
<connect gate="G$1" pin="A21" pad="53"/>
<connect gate="G$1" pin="A22" pad="54"/>
<connect gate="G$1" pin="A23" pad="55"/>
<connect gate="G$1" pin="A3" pad="34"/>
<connect gate="G$1" pin="A4" pad="35"/>
<connect gate="G$1" pin="A5" pad="36"/>
<connect gate="G$1" pin="A6" pad="37"/>
<connect gate="G$1" pin="A7" pad="38"/>
<connect gate="G$1" pin="A8" pad="39"/>
<connect gate="G$1" pin="A9" pad="40"/>
<connect gate="G$1" pin="AS" pad="6"/>
<connect gate="G$1" pin="BERR" pad="24"/>
<connect gate="G$1" pin="BG" pad="11"/>
<connect gate="G$1" pin="BGACK" pad="12"/>
<connect gate="G$1" pin="BR" pad="13"/>
<connect gate="G$1" pin="CLK" pad="15"/>
<connect gate="G$1" pin="D0" pad="5"/>
<connect gate="G$1" pin="D1" pad="4"/>
<connect gate="G$1" pin="D10" pad="63"/>
<connect gate="G$1" pin="D11" pad="62"/>
<connect gate="G$1" pin="D12" pad="61"/>
<connect gate="G$1" pin="D13" pad="60"/>
<connect gate="G$1" pin="D14" pad="59"/>
<connect gate="G$1" pin="D15" pad="58"/>
<connect gate="G$1" pin="D2" pad="3"/>
<connect gate="G$1" pin="D3" pad="2"/>
<connect gate="G$1" pin="D4" pad="1"/>
<connect gate="G$1" pin="D5" pad="68"/>
<connect gate="G$1" pin="D6" pad="67"/>
<connect gate="G$1" pin="D7" pad="66"/>
<connect gate="G$1" pin="D8" pad="65"/>
<connect gate="G$1" pin="D9" pad="64"/>
<connect gate="G$1" pin="DTACK" pad="10"/>
<connect gate="G$1" pin="E" pad="22"/>
<connect gate="G$1" pin="FC0" pad="30"/>
<connect gate="G$1" pin="FC1" pad="29"/>
<connect gate="G$1" pin="FC2" pad="28"/>
<connect gate="G$1" pin="HALT" pad="19"/>
<connect gate="G$1" pin="IPL0" pad="27"/>
<connect gate="G$1" pin="IPL1" pad="26"/>
<connect gate="G$1" pin="IPL2" pad="25"/>
<connect gate="G$1" pin="LDS" pad="8"/>
<connect gate="G$1" pin="R/W" pad="9"/>
<connect gate="G$1" pin="RESET" pad="20"/>
<connect gate="G$1" pin="UDS" pad="7"/>
<connect gate="G$1" pin="VMA" pad="21"/>
<connect gate="G$1" pin="VPA" pad="23"/>
<connect gate="P" pin="GND@1" pad="16"/>
<connect gate="P" pin="GND@2" pad="17"/>
<connect gate="P" pin="GND@3" pad="56"/>
<connect gate="P" pin="GND@4" pad="57"/>
<connect gate="P" pin="VCC@1" pad="14"/>
<connect gate="P" pin="VCC@2" pad="52"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MC68000P" prefix="IC" uservalue="yes">
<description>&lt;b&gt;68xxx PROCESSOR&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="68000" x="0" y="0"/>
<gate name="P" symbol="2PWR2GND" x="-40.64" y="12.7" addlevel="request"/>
</gates>
<devices>
<device name="" package="DIL64">
<connects>
<connect gate="G$1" pin="A1" pad="29"/>
<connect gate="G$1" pin="A10" pad="38"/>
<connect gate="G$1" pin="A11" pad="39"/>
<connect gate="G$1" pin="A12" pad="40"/>
<connect gate="G$1" pin="A13" pad="41"/>
<connect gate="G$1" pin="A14" pad="42"/>
<connect gate="G$1" pin="A15" pad="43"/>
<connect gate="G$1" pin="A16" pad="44"/>
<connect gate="G$1" pin="A17" pad="45"/>
<connect gate="G$1" pin="A18" pad="46"/>
<connect gate="G$1" pin="A19" pad="47"/>
<connect gate="G$1" pin="A2" pad="30"/>
<connect gate="G$1" pin="A20" pad="48"/>
<connect gate="G$1" pin="A21" pad="50"/>
<connect gate="G$1" pin="A22" pad="51"/>
<connect gate="G$1" pin="A23" pad="52"/>
<connect gate="G$1" pin="A3" pad="31"/>
<connect gate="G$1" pin="A4" pad="32"/>
<connect gate="G$1" pin="A5" pad="33"/>
<connect gate="G$1" pin="A6" pad="34"/>
<connect gate="G$1" pin="A7" pad="35"/>
<connect gate="G$1" pin="A8" pad="36"/>
<connect gate="G$1" pin="A9" pad="37"/>
<connect gate="G$1" pin="AS" pad="6"/>
<connect gate="G$1" pin="BERR" pad="22"/>
<connect gate="G$1" pin="BG" pad="11"/>
<connect gate="G$1" pin="BGACK" pad="12"/>
<connect gate="G$1" pin="BR" pad="13"/>
<connect gate="G$1" pin="CLK" pad="15"/>
<connect gate="G$1" pin="D0" pad="5"/>
<connect gate="G$1" pin="D1" pad="4"/>
<connect gate="G$1" pin="D10" pad="59"/>
<connect gate="G$1" pin="D11" pad="58"/>
<connect gate="G$1" pin="D12" pad="57"/>
<connect gate="G$1" pin="D13" pad="56"/>
<connect gate="G$1" pin="D14" pad="55"/>
<connect gate="G$1" pin="D15" pad="54"/>
<connect gate="G$1" pin="D2" pad="3"/>
<connect gate="G$1" pin="D3" pad="2"/>
<connect gate="G$1" pin="D4" pad="1"/>
<connect gate="G$1" pin="D5" pad="64"/>
<connect gate="G$1" pin="D6" pad="63"/>
<connect gate="G$1" pin="D7" pad="62"/>
<connect gate="G$1" pin="D8" pad="61"/>
<connect gate="G$1" pin="D9" pad="60"/>
<connect gate="G$1" pin="DTACK" pad="10"/>
<connect gate="G$1" pin="E" pad="20"/>
<connect gate="G$1" pin="FC0" pad="28"/>
<connect gate="G$1" pin="FC1" pad="27"/>
<connect gate="G$1" pin="FC2" pad="26"/>
<connect gate="G$1" pin="HALT" pad="17"/>
<connect gate="G$1" pin="IPL0" pad="25"/>
<connect gate="G$1" pin="IPL1" pad="24"/>
<connect gate="G$1" pin="IPL2" pad="23"/>
<connect gate="G$1" pin="LDS" pad="8"/>
<connect gate="G$1" pin="R/W" pad="9"/>
<connect gate="G$1" pin="RESET" pad="18"/>
<connect gate="G$1" pin="UDS" pad="7"/>
<connect gate="G$1" pin="VMA" pad="19"/>
<connect gate="G$1" pin="VPA" pad="21"/>
<connect gate="P" pin="GND@1" pad="16"/>
<connect gate="P" pin="GND@2" pad="53"/>
<connect gate="P" pin="VCC@1" pad="14"/>
<connect gate="P" pin="VCC@2" pad="49"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="68000_DIP64" library="micro-mc68000" deviceset="MC68000P" device=""/>
<part name="A600_CPU" library="micro-mc68000" deviceset="MC68000FN" device="TOPMOUNT"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="68000_DIP64" gate="G$1" x="25.4" y="48.26" smashed="yes">
<attribute name="VALUE" x="12.7" y="-5.08" size="1.778" layer="96"/>
<attribute name="NAME" x="12.7" y="97.155" size="1.778" layer="95"/>
</instance>
<instance part="A600_CPU" gate="G$1" x="68.58" y="48.26" smashed="yes" rot="MR0">
<attribute name="VALUE" x="81.28" y="-5.08" size="1.778" layer="96" rot="MR0"/>
<attribute name="NAME" x="81.28" y="97.155" size="1.778" layer="95" rot="MR0"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="FC0" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="FC0"/>
<wire x1="40.64" y1="93.98" x2="53.34" y2="93.98" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="FC0"/>
</segment>
</net>
<net name="FC1" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="FC1"/>
<wire x1="53.34" y1="91.44" x2="40.64" y2="91.44" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="FC1"/>
</segment>
</net>
<net name="FC2" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="FC2"/>
<wire x1="40.64" y1="88.9" x2="53.34" y2="88.9" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="FC2"/>
</segment>
</net>
<net name="E" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="E"/>
<wire x1="53.34" y1="81.28" x2="40.64" y2="81.28" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="E"/>
</segment>
</net>
<net name="VMA" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="VMA"/>
<wire x1="40.64" y1="78.74" x2="53.34" y2="78.74" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="VMA"/>
</segment>
</net>
<net name="BG" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="BG"/>
<wire x1="53.34" y1="73.66" x2="40.64" y2="73.66" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="BG"/>
</segment>
</net>
<net name="AS" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="AS"/>
<wire x1="40.64" y1="68.58" x2="53.34" y2="68.58" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="AS"/>
</segment>
</net>
<net name="RW" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="R/W"/>
<wire x1="53.34" y1="66.04" x2="40.64" y2="66.04" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="R/W"/>
</segment>
</net>
<net name="UDS" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="UDS"/>
<wire x1="40.64" y1="63.5" x2="53.34" y2="63.5" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="UDS"/>
</segment>
</net>
<net name="LDS" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="LDS"/>
<wire x1="53.34" y1="60.96" x2="40.64" y2="60.96" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="LDS"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A1"/>
<wire x1="40.64" y1="55.88" x2="53.34" y2="55.88" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A1"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A2"/>
<wire x1="53.34" y1="53.34" x2="40.64" y2="53.34" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A2"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A3"/>
<wire x1="40.64" y1="50.8" x2="53.34" y2="50.8" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A3"/>
</segment>
</net>
<net name="A4" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A4"/>
<wire x1="53.34" y1="48.26" x2="40.64" y2="48.26" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A4"/>
</segment>
</net>
<net name="A5" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A5"/>
<wire x1="40.64" y1="45.72" x2="53.34" y2="45.72" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A5"/>
</segment>
</net>
<net name="A6" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A6"/>
<wire x1="53.34" y1="43.18" x2="40.64" y2="43.18" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A6"/>
</segment>
</net>
<net name="A7" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A7"/>
<wire x1="40.64" y1="40.64" x2="53.34" y2="40.64" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A7"/>
</segment>
</net>
<net name="A8" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A8"/>
<wire x1="53.34" y1="38.1" x2="40.64" y2="38.1" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A8"/>
</segment>
</net>
<net name="A9" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A9"/>
<wire x1="40.64" y1="35.56" x2="53.34" y2="35.56" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A9"/>
</segment>
</net>
<net name="A10" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A10"/>
<wire x1="53.34" y1="33.02" x2="40.64" y2="33.02" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A10"/>
</segment>
</net>
<net name="A11" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A11"/>
<wire x1="40.64" y1="30.48" x2="53.34" y2="30.48" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A11"/>
</segment>
</net>
<net name="A12" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A12"/>
<wire x1="53.34" y1="27.94" x2="40.64" y2="27.94" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A12"/>
</segment>
</net>
<net name="A13" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A13"/>
<wire x1="40.64" y1="25.4" x2="53.34" y2="25.4" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A13"/>
</segment>
</net>
<net name="A14" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A14"/>
<wire x1="53.34" y1="22.86" x2="40.64" y2="22.86" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A14"/>
</segment>
</net>
<net name="A15" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A15"/>
<wire x1="40.64" y1="20.32" x2="53.34" y2="20.32" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A15"/>
</segment>
</net>
<net name="A16" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A16"/>
<wire x1="53.34" y1="17.78" x2="40.64" y2="17.78" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A16"/>
</segment>
</net>
<net name="A17" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A17"/>
<wire x1="40.64" y1="15.24" x2="53.34" y2="15.24" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A17"/>
</segment>
</net>
<net name="A18" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A18"/>
<wire x1="53.34" y1="12.7" x2="40.64" y2="12.7" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A18"/>
</segment>
</net>
<net name="A19" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A19"/>
<wire x1="53.34" y1="10.16" x2="40.64" y2="10.16" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A19"/>
</segment>
</net>
<net name="A20" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A20"/>
<wire x1="40.64" y1="7.62" x2="53.34" y2="7.62" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A20"/>
</segment>
</net>
<net name="A21" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A21"/>
<wire x1="53.34" y1="5.08" x2="40.64" y2="5.08" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A21"/>
</segment>
</net>
<net name="A22" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A22"/>
<wire x1="40.64" y1="2.54" x2="53.34" y2="2.54" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A22"/>
</segment>
</net>
<net name="A23" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="A23"/>
<wire x1="53.34" y1="0" x2="40.64" y2="0" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="A23"/>
</segment>
</net>
<net name="D15" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="D15"/>
<wire x1="7.62" y1="0" x2="5.08" y2="0" width="0.1524" layer="91"/>
<wire x1="5.08" y1="0" x2="5.08" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="5.08" y1="-5.08" x2="88.9" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="88.9" y1="-5.08" x2="88.9" y2="0" width="0.1524" layer="91"/>
<wire x1="88.9" y1="0" x2="86.36" y2="0" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="D15"/>
</segment>
</net>
<net name="D14" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="D14"/>
<wire x1="7.62" y1="2.54" x2="2.54" y2="2.54" width="0.1524" layer="91"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="2.54" y1="-7.62" x2="91.44" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-7.62" x2="91.44" y2="2.54" width="0.1524" layer="91"/>
<wire x1="91.44" y1="2.54" x2="86.36" y2="2.54" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="D14"/>
</segment>
</net>
<net name="D13" class="0">
<segment>
<wire x1="86.36" y1="5.08" x2="93.98" y2="5.08" width="0.1524" layer="91"/>
<wire x1="93.98" y1="5.08" x2="93.98" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-10.16" x2="0" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="0" y1="-10.16" x2="0" y2="5.08" width="0.1524" layer="91"/>
<pinref part="68000_DIP64" gate="G$1" pin="D13"/>
<wire x1="0" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="D13"/>
</segment>
</net>
<net name="D12" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="D12"/>
<wire x1="7.62" y1="7.62" x2="-2.54" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="7.62" x2="-2.54" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="-12.7" x2="96.52" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-12.7" x2="96.52" y2="7.62" width="0.1524" layer="91"/>
<wire x1="96.52" y1="7.62" x2="86.36" y2="7.62" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="D12"/>
</segment>
</net>
<net name="D11" class="0">
<segment>
<wire x1="86.36" y1="10.16" x2="99.06" y2="10.16" width="0.1524" layer="91"/>
<wire x1="99.06" y1="10.16" x2="99.06" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-15.24" x2="-5.08" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="-15.24" x2="-5.08" y2="10.16" width="0.1524" layer="91"/>
<pinref part="68000_DIP64" gate="G$1" pin="D11"/>
<wire x1="-5.08" y1="10.16" x2="7.62" y2="10.16" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="D11"/>
</segment>
</net>
<net name="D10" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="D10"/>
<wire x1="7.62" y1="12.7" x2="-7.62" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="12.7" x2="-7.62" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="-17.78" x2="101.6" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-17.78" x2="101.6" y2="12.7" width="0.1524" layer="91"/>
<wire x1="101.6" y1="12.7" x2="86.36" y2="12.7" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="D10"/>
</segment>
</net>
<net name="D9" class="0">
<segment>
<wire x1="86.36" y1="15.24" x2="104.14" y2="15.24" width="0.1524" layer="91"/>
<wire x1="104.14" y1="15.24" x2="104.14" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-20.32" x2="-10.16" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-20.32" x2="-10.16" y2="15.24" width="0.1524" layer="91"/>
<pinref part="68000_DIP64" gate="G$1" pin="D9"/>
<wire x1="-10.16" y1="15.24" x2="7.62" y2="15.24" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="D9"/>
</segment>
</net>
<net name="D8" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="D8"/>
<wire x1="7.62" y1="17.78" x2="-12.7" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="17.78" x2="-12.7" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="-22.86" x2="106.68" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="106.68" y1="-22.86" x2="106.68" y2="17.78" width="0.1524" layer="91"/>
<wire x1="106.68" y1="17.78" x2="86.36" y2="17.78" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="D8"/>
</segment>
</net>
<net name="D7" class="0">
<segment>
<wire x1="86.36" y1="20.32" x2="109.22" y2="20.32" width="0.1524" layer="91"/>
<wire x1="109.22" y1="20.32" x2="109.22" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-25.4" x2="-15.24" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="-25.4" x2="-15.24" y2="20.32" width="0.1524" layer="91"/>
<pinref part="68000_DIP64" gate="G$1" pin="D7"/>
<wire x1="-15.24" y1="20.32" x2="7.62" y2="20.32" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="D7"/>
</segment>
</net>
<net name="D6" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="D6"/>
<wire x1="7.62" y1="22.86" x2="-17.78" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="22.86" x2="-17.78" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="-27.94" x2="111.76" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="111.76" y1="-27.94" x2="111.76" y2="22.86" width="0.1524" layer="91"/>
<wire x1="111.76" y1="22.86" x2="86.36" y2="22.86" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="D6"/>
</segment>
</net>
<net name="D5" class="0">
<segment>
<wire x1="86.36" y1="25.4" x2="114.3" y2="25.4" width="0.1524" layer="91"/>
<wire x1="114.3" y1="25.4" x2="114.3" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="114.3" y1="-30.48" x2="-20.32" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-30.48" x2="-20.32" y2="25.4" width="0.1524" layer="91"/>
<pinref part="68000_DIP64" gate="G$1" pin="D5"/>
<wire x1="-20.32" y1="25.4" x2="7.62" y2="25.4" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="D5"/>
</segment>
</net>
<net name="D4" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="D4"/>
<wire x1="7.62" y1="27.94" x2="-22.86" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="27.94" x2="-22.86" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-33.02" x2="116.84" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="116.84" y1="-33.02" x2="116.84" y2="27.94" width="0.1524" layer="91"/>
<wire x1="116.84" y1="27.94" x2="86.36" y2="27.94" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="D4"/>
</segment>
</net>
<net name="D3" class="0">
<segment>
<wire x1="86.36" y1="30.48" x2="119.38" y2="30.48" width="0.1524" layer="91"/>
<wire x1="119.38" y1="30.48" x2="119.38" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-35.56" x2="-25.4" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="-35.56" x2="-25.4" y2="30.48" width="0.1524" layer="91"/>
<pinref part="68000_DIP64" gate="G$1" pin="D3"/>
<wire x1="-25.4" y1="30.48" x2="7.62" y2="30.48" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="D3"/>
</segment>
</net>
<net name="D2" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="D2"/>
<wire x1="7.62" y1="33.02" x2="-27.94" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="33.02" x2="-27.94" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="-38.1" x2="121.92" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="121.92" y1="-38.1" x2="121.92" y2="33.02" width="0.1524" layer="91"/>
<wire x1="121.92" y1="33.02" x2="86.36" y2="33.02" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="D2"/>
</segment>
</net>
<net name="D1" class="0">
<segment>
<wire x1="86.36" y1="35.56" x2="124.46" y2="35.56" width="0.1524" layer="91"/>
<wire x1="124.46" y1="35.56" x2="124.46" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="124.46" y1="-40.64" x2="-30.48" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="-40.64" x2="-30.48" y2="35.56" width="0.1524" layer="91"/>
<pinref part="68000_DIP64" gate="G$1" pin="D1"/>
<wire x1="-30.48" y1="35.56" x2="7.62" y2="35.56" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="D1"/>
</segment>
</net>
<net name="D0" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="D0"/>
<wire x1="7.62" y1="38.1" x2="-33.02" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="38.1" x2="-33.02" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="-33.02" y1="-43.18" x2="127" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="127" y1="-43.18" x2="127" y2="38.1" width="0.1524" layer="91"/>
<wire x1="127" y1="38.1" x2="86.36" y2="38.1" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="D0"/>
</segment>
</net>
<net name="CLK" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="CLK"/>
<wire x1="7.62" y1="93.98" x2="5.08" y2="93.98" width="0.1524" layer="91"/>
<wire x1="5.08" y1="93.98" x2="5.08" y2="101.6" width="0.1524" layer="91"/>
<wire x1="5.08" y1="101.6" x2="88.9" y2="101.6" width="0.1524" layer="91"/>
<wire x1="88.9" y1="101.6" x2="88.9" y2="93.98" width="0.1524" layer="91"/>
<wire x1="88.9" y1="93.98" x2="86.36" y2="93.98" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="CLK"/>
</segment>
</net>
<net name="BERR" class="0">
<segment>
<wire x1="86.36" y1="88.9" x2="91.44" y2="88.9" width="0.1524" layer="91"/>
<wire x1="91.44" y1="88.9" x2="91.44" y2="104.14" width="0.1524" layer="91"/>
<wire x1="91.44" y1="104.14" x2="2.54" y2="104.14" width="0.1524" layer="91"/>
<wire x1="2.54" y1="104.14" x2="2.54" y2="88.9" width="0.1524" layer="91"/>
<pinref part="68000_DIP64" gate="G$1" pin="BERR"/>
<wire x1="2.54" y1="88.9" x2="7.62" y2="88.9" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="BERR"/>
</segment>
</net>
<net name="RST" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="RESET"/>
<wire x1="7.62" y1="86.36" x2="0" y2="86.36" width="0.1524" layer="91"/>
<wire x1="0" y1="86.36" x2="0" y2="106.68" width="0.1524" layer="91"/>
<wire x1="0" y1="106.68" x2="93.98" y2="106.68" width="0.1524" layer="91"/>
<wire x1="93.98" y1="106.68" x2="93.98" y2="86.36" width="0.1524" layer="91"/>
<wire x1="93.98" y1="86.36" x2="86.36" y2="86.36" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="RESET"/>
</segment>
</net>
<net name="VPA" class="0">
<segment>
<wire x1="86.36" y1="78.74" x2="99.06" y2="78.74" width="0.1524" layer="91"/>
<wire x1="99.06" y1="78.74" x2="99.06" y2="111.76" width="0.1524" layer="91"/>
<wire x1="99.06" y1="111.76" x2="-5.08" y2="111.76" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="111.76" x2="-5.08" y2="78.74" width="0.1524" layer="91"/>
<pinref part="68000_DIP64" gate="G$1" pin="VPA"/>
<wire x1="-5.08" y1="78.74" x2="7.62" y2="78.74" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="VPA"/>
</segment>
</net>
<net name="BR" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="BR"/>
<wire x1="7.62" y1="73.66" x2="-7.62" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="73.66" x2="-7.62" y2="114.3" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="114.3" x2="101.6" y2="114.3" width="0.1524" layer="91"/>
<wire x1="101.6" y1="114.3" x2="101.6" y2="73.66" width="0.1524" layer="91"/>
<wire x1="101.6" y1="73.66" x2="86.36" y2="73.66" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="BR"/>
</segment>
</net>
<net name="BGACK" class="0">
<segment>
<wire x1="86.36" y1="71.12" x2="104.14" y2="71.12" width="0.1524" layer="91"/>
<wire x1="104.14" y1="71.12" x2="104.14" y2="116.84" width="0.1524" layer="91"/>
<wire x1="104.14" y1="116.84" x2="-10.16" y2="116.84" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="116.84" x2="-10.16" y2="71.12" width="0.1524" layer="91"/>
<pinref part="68000_DIP64" gate="G$1" pin="BGACK"/>
<wire x1="-10.16" y1="71.12" x2="7.62" y2="71.12" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="BGACK"/>
</segment>
</net>
<net name="DTACK" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="DTACK"/>
<wire x1="7.62" y1="66.04" x2="-12.7" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="66.04" x2="-12.7" y2="119.38" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="119.38" x2="106.68" y2="119.38" width="0.1524" layer="91"/>
<wire x1="106.68" y1="119.38" x2="106.68" y2="66.04" width="0.1524" layer="91"/>
<wire x1="106.68" y1="66.04" x2="86.36" y2="66.04" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="DTACK"/>
</segment>
</net>
<net name="IPL0" class="0">
<segment>
<wire x1="86.36" y1="60.96" x2="109.22" y2="60.96" width="0.1524" layer="91"/>
<wire x1="109.22" y1="60.96" x2="109.22" y2="121.92" width="0.1524" layer="91"/>
<wire x1="109.22" y1="121.92" x2="-15.24" y2="121.92" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="121.92" x2="-15.24" y2="60.96" width="0.1524" layer="91"/>
<pinref part="68000_DIP64" gate="G$1" pin="IPL0"/>
<wire x1="-15.24" y1="60.96" x2="7.62" y2="60.96" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="IPL0"/>
</segment>
</net>
<net name="IPL1" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="IPL1"/>
<wire x1="7.62" y1="58.42" x2="-17.78" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="58.42" x2="-17.78" y2="124.46" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="124.46" x2="111.76" y2="124.46" width="0.1524" layer="91"/>
<wire x1="111.76" y1="124.46" x2="111.76" y2="58.42" width="0.1524" layer="91"/>
<wire x1="111.76" y1="58.42" x2="86.36" y2="58.42" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="IPL1"/>
</segment>
</net>
<net name="IPL2" class="0">
<segment>
<wire x1="86.36" y1="55.88" x2="114.3" y2="55.88" width="0.1524" layer="91"/>
<wire x1="114.3" y1="55.88" x2="114.3" y2="127" width="0.1524" layer="91"/>
<wire x1="114.3" y1="127" x2="-20.32" y2="127" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="127" x2="-20.32" y2="55.88" width="0.1524" layer="91"/>
<pinref part="68000_DIP64" gate="G$1" pin="IPL2"/>
<wire x1="-20.32" y1="55.88" x2="7.62" y2="55.88" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="IPL2"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="68000_DIP64" gate="G$1" pin="HALT"/>
<wire x1="7.62" y1="83.82" x2="-2.54" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="83.82" x2="-2.54" y2="109.22" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="109.22" x2="96.52" y2="109.22" width="0.1524" layer="91"/>
<wire x1="96.52" y1="109.22" x2="96.52" y2="83.82" width="0.1524" layer="91"/>
<pinref part="A600_CPU" gate="G$1" pin="HALT"/>
<wire x1="96.52" y1="83.82" x2="86.36" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
